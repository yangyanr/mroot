<#-- /* 头部标题 */ -->
<@OVERRIDE name="HEAD_TITLE">
    <#if headTitle??>
        <title>${headTitle}</title>
    <#else>
        <title>${I18N("message.blog.metaTitle")}</title>
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_KEYWORDS">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content??>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content}">
    <#else>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud">
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_DESCRIPTION">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content??>
        <meta name="description"
              content="${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content},${I18N("message.blog.meta.base")}">
    <#else>
        <meta name="description"
              content="IT网站,技术博客,${I18N("message.blog.meta.base")}">
    </#if>
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="row ">

        <div class="col-lg-9 pt-1 pb-5 mb-3">
            <#if page.records??  && (0 < page.records?size)>

                <#list page.records as item>
                    <div class="row mt-2 mb-2">
                        <article class="blog-post col">
                            <div class="row">

                                <div class="col-sm-8 col-lg-5">
                                    <div class="blog-post-image-wrapper img-thumbnail img-thumbnail-no-borders d-block">
                                        <#--                                        <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"-->
                                        <#--                                           title="${item.title}"-->
                                        <#--                                           target="_blank">-->
                                        <#if item.cover??>
                                            <img width="600" height="390" class="img-fluid mb-4"
                                                 src="${item.cover}"
                                                 alt="${item.title}">
                                        <#else>
                                            <img width="600" height="390" class="img-fluid mb-4"
                                                 src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                                 alt="${item.title}">
                                        </#if>
                                        <#--                                        </a>-->
                                        <span class="blog-post-date bg-color-primary text-color-light font-weight-bold"><span><@dateYearFormat  item.gmtCreate></@dateYearFormat></span><span
                                                    class="month-year font-weight-light"><@dateDayFormat  item.gmtCreate></@dateDayFormat></span>
												</span>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-7">
                                    <h4><a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                           target="_blank">${item.title}</a></h4>
                                    <p>${item.description}</p>
                                    <hr class="gratient">
                                    <div class="post-infos d-flex">
												<span class="info posted-by">
                                                    <span>${I18N("message.blog.list.article.category")}</span>
													<span class="post-author font-weight-semibold text-color-dark"><@defaultStr item.category.title></@defaultStr></span>
												</span>
                                        <span class="info like ml-5"><span>${I18N("message.blog.list.article.pageView")}</span>
													<span class="like-number font-weight-semibold custom-color-red"><@defaultStr item.pageView></@defaultStr></span>
												</span>
                                    </div>
                                    <a class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mt-5"
                                       href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                       title="${I18N("message.blog.list.article.detail")}"
                                       target="_blank"><span>${I18N("message.blog.list.article.detail")}</span></a>
                                </div>

                            </div>
                        </article>
                    </div>

                    <hr class="solid tall mt-5">

                </#list>

                <div class="row text-center mt-5">
                    <div class="col">
                        <a class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase"
                           href="${CONTEXT_PATH}/cms/category/index"
                           target="_blank"><span>${I18N("message.blog.list.article.more")}</span></a>
                    </div>
                </div>

            <#else>

                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">${I18N("message.page.empty.content")}</h4>
                    </div>
                </div>

            </#if>
        </div>

        <div class="col-lg-3 mt-4 mt-lg-0 appear-animation" data-appear-animation="fadeIn"
             data-appear-animation-delay="300">

            <aside class="sidebar">
                <#if hotList??  && (0 < hotList?size)>
                    <div class="tabs tabs-dark mb-4 pb-2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a class="nav-link text-1 font-weight-bold text-uppercase"
                                                    href="#recentPosts"
                                                    data-toggle="tab">${I18N("message.blog.index.list.view.hot")}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="recentPosts">
                                <ul class="simple-post-list">
                                    <#list hotList as item>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                    <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                                       target="_blank">
                                                        <#if item.cover??>
                                                            <img width="50" height="50" class="img-fluid"
                                                                 src="${item.cover}"
                                                                 alt="${item.title}">
                                                        <#else>
                                                            <img width="50" height="50" class="img-fluid"
                                                                 src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                                                 alt="${item.title}">
                                                        </#if>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                                   class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                                   title="<@defaultStr item.title></@defaultStr>"><@subStr str=item.title length=item.title?length></@subStr></a>
                                                <div class="post-meta"><@dateFormat2  item.gmtCreate></@dateFormat2></div>
                                            </div>
                                        </li>
                                    </#list>
                                </ul>
                            </div>
                        </div>
                    </div>
                </#if>
                <h5 class="font-weight-bold pt-4">编程中一个迷途程序猿的分享</h5>
                <p>技术分享，分享提升自己，分享让世界更美好。</p>
            </aside>

        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">

    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            BlogTool.highlight_top_nav('${navIndex}');
        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
