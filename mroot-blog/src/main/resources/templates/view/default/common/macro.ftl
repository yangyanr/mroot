<#-- 页面用到的函数 -->

<#-- 默认值 -->
<#macro defaultStr str>
    <#if str??>${str}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateFormat date>
    <#if date??>${date?string("yyyy-MM-dd HH:mm:ss SSS")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateFormat2 date>
    <#if date??>${date?string("yyyy-MM-dd HH:mm:ss")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro number2DateFormat date>
    <#if date??>${date?number_to_datetime}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateYearFormat date>
    <#if date??>${date?string("yy")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateDayFormat date>
    <#if date??>${date?string("MM-dd")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 列表数据 -->
<#macro listPage>

    <div class="col-lg-9 pt-1 pb-5 mb-3">
        <#if page.records??  && (0 < page.records?size)>

            <#list page.records as item>
                <div class="row mt-2 mb-2">
                    <article class="blog-post col">
                        <div class="row">

                            <div class="col-sm-8 col-lg-5">
                                <div class="blog-post-image-wrapper img-thumbnail img-thumbnail-no-borders d-block">
                                    <#--                                    <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"-->
                                    <#--                                       title="${item.title}"-->
                                    <#--                                       target="_blank">-->
                                    <#if item.cover??>
                                        <img width="600" height="390" class="img-fluid mb-4"
                                             src="${item.cover}"
                                             alt="${item.title}">
                                    <#else>
                                        <img width="600" height="390" class="img-fluid mb-4"
                                             src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                             alt="${item.title}">
                                    </#if>
                                    <#--                                    </a>-->
                                    <span class="blog-post-date bg-color-primary text-color-light font-weight-bold"><span><@dateYearFormat  item.gmtCreate></@dateYearFormat></span><span
                                                class="month-year font-weight-light"><@dateDayFormat  item.gmtCreate></@dateDayFormat></span>
												</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-7">
                                <h4><a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                       target="_blank">${item.title}</a></h4>
                                <p>${item.description}</p>
                                <hr class="gratient">
                                <div class="post-infos d-flex">
												<span class="info posted-by">
                                                    <span>${I18N("message.blog.list.article.category")}</span>
													<span class="post-author font-weight-semibold text-color-dark"><@defaultStr item.category.title></@defaultStr></span>
												</span>
                                    <span class="info like ml-5"><span>${I18N("message.blog.list.article.pageView")}</span>
													<span class="like-number font-weight-semibold custom-color-red"><@defaultStr item.pageView></@defaultStr></span>
												</span>
                                </div>
                                <a class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mt-5"
                                   href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                   title="${I18N("message.blog.list.article.detail")}"
                                   target="_blank"><span>${I18N("message.blog.list.article.detail")}</span></a>
                            </div>

                        </div>
                    </article>
                </div>

                <hr class="solid tall mt-5">

            </#list>

        <#else>

            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title">${I18N("message.page.empty.content")}</h4>
                </div>
            </div>

        </#if>
    </div>

    <div class="col-lg-3 mt-4 mt-lg-0 appear-animation" data-appear-animation="fadeIn"
         data-appear-animation-delay="300">

        <aside class="sidebar">

            <#if hotList??  && (0 < hotList?size)>
                <div class="tabs tabs-dark mb-4 pb-2">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link text-1 font-weight-bold text-uppercase"
                                                href="#recentPosts"
                                                data-toggle="tab">${I18N("message.blog.index.list.view.hot")}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="recentPosts">
                            <ul class="simple-post-list">
                                <#list hotList as item>
                                    <li>
                                        <div class="post-image">
                                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                                   target="_blank">
                                                    <#if item.cover??>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${item.cover}"
                                                             alt="${item.title}">
                                                    <#else>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                                             alt="${item.title}">
                                                    </#if>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="post-info">
                                            <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                               class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                               title="<@defaultStr item.title></@defaultStr>"><@subStr str=item.title length=item.title?length></@subStr></a>
                                            <div class="post-meta"><@dateFormat2  item.gmtCreate></@dateFormat2></div>
                                        </div>
                                    </li>
                                </#list>
                            </ul>
                        </div>
                    </div>
                </div>
            </#if>

            <h5 class="font-weight-bold pt-4">编程中一个迷途程序猿的分享</h5>
            <p>技术分享，分享提升自己，分享让世界更美好。</p>
        </aside>

    </div>

</#macro>

<#-- 截取字符串 -->
<#macro subStr str length>
    <#if str??>
        <#if (length >= str?length)>
            <#if (length >= 10)>
                ${str?substring(0,10)}...
            <#else>
                ${str}
            </#if>
        <#else>
            ${str?substring(0,length)}...
        </#if>
    <#else>${I18N("message.default.content")}</#if>
</#macro>


<#-- 金额 格式 1,000.00-->
<#macro moneyFormat money><#if money??>${money?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 金额 格式1000.00-->
<#macro moneyFormatUnComma money><#if money??>${money?string("0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字带0 -->
<#macro numberFormat number><#if number??>${number?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字 -->
<#macro numberFormatHasZero number><#if number??>${number?string(",##0")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 是/否 -->
<#macro boole value>
    <#if 1 == value>
        ${I18N("message.table.true.text")}
    <#else>
        ${I18N("message.table.false.text")}</#if>
</#macro>

<#-- i18n -->
<#macro i18nType i18n>
    <#if i18n??><#if ("zh" == language)>'zh-CN'<#else>'${i18n}'</#if><#else>'zh-CN'</#if>
</#macro>
