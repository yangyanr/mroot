/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.qiniu.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import static wang.encoding.mroot.plugin.qiniu.constant.QiNiuConst.QINIU_PREFIX;

/**
 * 七牛配置
 *
 * @author ErYang
 */
@Component
@PropertySource("classpath:qiniu.properties")
@ConfigurationProperties(prefix = QINIU_PREFIX)
@EnableCaching
@Getter
@Setter
public class QiNiuConst {


    /**
     * 七牛 配置前缀
     */
    public static final String QINIU_PREFIX = "qiniu";

    /**
     * AK
     */
    private String accessKey;
    /**
     * SK
     */
    private String secretKey;
    /**
     * 七牛机房
     */
    private String zone;
    /**
     * 七牛存储空间名称
     */
    private String bucket;
    /**
     * 七牛存储空间地址
     */
    private String baseUrl;
    /**
     * 上传目录前缀
     */
    private String uploadDirPrefix;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QiNiuConst class

/* End of file QiNiuConst.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/qiniu/constant/QiNiuConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
