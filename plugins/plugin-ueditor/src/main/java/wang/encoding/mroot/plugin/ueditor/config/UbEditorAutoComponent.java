/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.config;


import javax.annotation.PostConstruct;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.FastJsonUtils;
import wang.encoding.mroot.common.util.QiNiuUtils;
import wang.encoding.mroot.plugin.qiniu.constant.QiNiuConst;
import wang.encoding.mroot.plugin.ueditor.ActionEnter;
import wang.encoding.mroot.plugin.ueditor.ConfigManager;
import wang.encoding.mroot.plugin.ueditor.hunter.FileManager;
import wang.encoding.mroot.plugin.ueditor.upload.StorageManager;
import wang.encoding.mroot.service.admin.system.AdminConfigService;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;

import java.util.Map;
import java.util.Objects;


/**
 * 配置
 *
 * @author ErYang
 */
@Component
@EnableConfigurationProperties(UbEditorProperties.class)
@ConditionalOnClass(ActionEnter.class)
@ConditionalOnProperty(prefix = "ueditor", value = "enabled", matchIfMissing = true)
public class UbEditorAutoComponent {

    private static final Logger logger = LoggerFactory.getLogger(UbEditorAutoComponent.class);

    @Value(value = "${upload.resources-path}")
    protected String uploadResourcesPath;

    private final UbEditorProperties ueditorProperties;

    private final AdminConfigService adminConfigService;

    private final QiNiuConst qiNiuConst;

    private final DatabaseConst databaseConst;

    @Autowired(required = false)
    @Lazy
    public UbEditorAutoComponent(UbEditorProperties ueditorProperties, DatabaseConst databaseConst,
            AdminConfigService adminConfigService, QiNiuConst qiNiuConst) {
        this.ueditorProperties = ueditorProperties;
        this.databaseConst = databaseConst;
        this.adminConfigService = adminConfigService;
        this.qiNiuConst = qiNiuConst;
    }

    /**
     * 注册 bean
     *
     * @return ActionEnter
     */
    @Bean
    @ConditionalOnMissingBean(ActionEnter.class)
    public ActionEnter actionEnter() {
        return new ActionEnter(new ConfigManager(ueditorProperties.getConfig()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 注入配置
     */
    @PostConstruct
    public void storageManager() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置百度编辑器插件的七牛配置开始<<<<<<<<");
        }
        // 是否启用七牛上传
        boolean qiNiuUpload = false;
        // 得到数据库 七牛 的配置
        AdminConfigGetVO configVO = adminConfigService.getBySole(databaseConst.getQiniuUploadName());
        if (null != configVO) {
            if (Objects.equals(ConfigTypeEnum.FUN.getValue(), configVO.getType())) {
                if (null != configVO.getContent() && StringUtils.isNotBlank(configVO.getContent())) {
                    Map map = FastJsonUtils.parseJson2Map(configVO.getContent());
                    if (null != map && !map.isEmpty()) {
                        String status = (String) map.get("status");
                        if (String.valueOf(StateEnum.NORMAL.getKey()).equals(status)) {
                            qiNiuUpload = true;
                        }
                    }
                }
            }
        }

        StorageManager.accessKey = FileManager.accessKey = qiNiuConst.getAccessKey();
        StorageManager.secretKey = FileManager.secretKey = qiNiuConst.getSecretKey();
        StorageManager.baseUrl = FileManager.baseUrl = qiNiuConst.getBaseUrl();
        StorageManager.bucket = FileManager.bucket = qiNiuConst.getBucket();
        StorageManager.uploadDirPrefix = FileManager.uploadDirPrefix = qiNiuConst.getUploadDirPrefix();
        StorageManager.zone = FileManager.zone = QiNiuUtils.ZONE;
        StorageManager.uploadLocal = FileManager.uploadLocal = !qiNiuUpload;
        StorageManager.uploadResourcesPath = FileManager.uploadResourcesPath = uploadResourcesPath;
        QiNiuUtils.uploadLocal = !qiNiuUpload;
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置百度编辑器插件的七牛配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UbEditorAutoComponent class

/* End of file UbEditorAutoComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/config/UbEditorAutoComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
