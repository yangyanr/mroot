/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.CharsetsUtils;
import wang.encoding.mroot.common.util.text.EncodeUtils;
import wang.encoding.mroot.common.util.ExceptionUtils;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 封装 md5 sha Hash 算法的工具类
 * 摘要加密工具类(非可逆加密)
 *
 * @author ErYang
 */
public class DigestUtils {


    private static final Logger logger = LoggerFactory.getLogger(DigestUtils.class);

    /**
     * MD5 标识
     */
    private static final String MD5 = "MD5";
    /**
     * SHA-1
     */
    private static final String SHA1 = "SHA-1";
    /**
     * SHA-256
     */
    private static final String SHA256 = "SHA-256";
    /**
     * SHA-512
     */
    private static final String SHA512 = "SHA-512";
    /**
     * MD5 标识
     */
    private static final ThreadLocal<MessageDigest> MD5_DIGEST = createThreadLocalMessageDigest(MD5);
    /**
     * SHA-1 标识
     */
    private static final ThreadLocal<MessageDigest> SHA1_DIGEST = createThreadLocalMessageDigest(SHA1);
    /**
     * SHA-256 标识
     */
    private static final ThreadLocal<MessageDigest> SHA256_DIGEST = createThreadLocalMessageDigest(SHA256);
    /**
     * SHA-512 标识
     */
    private static final ThreadLocal<MessageDigest> SHA512_DIGEST = createThreadLocalMessageDigest(SHA512);


    /***
     * 实现 MD5 加密
     * @param input byte[] 需要加密的字符
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final byte[] input) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(MD5_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 MD5 加密
     * @param input String 需要加密的字符串
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final String input) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(MD5_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 MD5 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final byte[] input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(MD5_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 MD5 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final String input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(MD5_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 MD5 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final byte[] input, @Nullable final byte[] salt,
                                @NotNull final Integer iterations) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(MD5_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 MD5 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getMd5(@NotNull final String input, @Nullable final byte[] salt,
                                @NotNull final Integer iterations) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(MD5_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密
     * @param input byte[] 需要加密的字符
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final byte[] input) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA1_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密
     * @param input String 需要加密的字符串
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final String input) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA1_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final byte[] input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA1_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final String input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA1_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final byte[] input, @Nullable final byte[] salt,
                                 @NotNull final Integer iterations) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA1_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-1 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha1(@NotNull final String input, @Nullable final byte[] salt,
                                 @NotNull final Integer iterations) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA1_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密
     * @param input byte[] 需要加密的字符
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final byte[] input) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA256_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密
     * @param input String 需要加密的字符串
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final String input) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA256_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final byte[] input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA256_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final String input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA256_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final byte[] input, @Nullable final byte[] salt,
                                   @NotNull final Integer iterations) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA256_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-256 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha256(@NotNull final String input, @Nullable final byte[] salt,
                                   @NotNull final Integer iterations) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA256_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密
     * @param input byte[] 需要加密的字符
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final byte[] input) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA512_DIGEST), null, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final byte[] input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA512_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt String 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final byte[] input, final @NotNull String salt) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA512_DIGEST),
                salt.getBytes(CharsetsUtils.UTF_8), 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final String input, @Nullable final byte[] salt) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA512_DIGEST), salt, 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt String带盐
     * @return String 加密后的报文
     */
    public static byte[] sha512(@NotNull final String input, @NotNull final String salt) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA512_DIGEST),
                salt.getBytes(CharsetsUtils.UTF_8), 1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt String 带盐
     * @return String Base32Hex 编码 加密后的报文
     */
    public static String getSha512(@NotNull final String input, @NotNull final String salt) {
        return EncodeUtils.encodeBase32Hex(DigestUtils.sha512(input, salt));
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性.
     * @param input byte[] 需要加密的字符
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final byte[] input, @Nullable final byte[] salt,
                                   @NotNull final Integer iterations) {
        return DigestUtils.digest(input, DigestUtils.getMessageDigest(SHA512_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 实现 SHA-512 加密 带salt达到更高的安全性
     * @param input String 需要加密的字符串
     * @param salt byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return String 加密后的报文
     */
    public static byte[] getSha512(@NotNull final String input, @Nullable final byte[] salt,
                                   @NotNull final Integer iterations) {
        return DigestUtils.digest(input.getBytes(CharsetsUtils.UTF_8),
                DigestUtils.getMessageDigest(SHA512_DIGEST), salt, iterations);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对字符串进行散列 支持md5与sha1算法 实现加密
     *
     * @param input      byte[] 需要加密的内容
     * @param digest     MessageDigest 加密类型
     * @param salt       byte[] 带盐
     * @param iterations Integer 迭代次数
     * @return byte[] 加密后的报文
     */
    private static byte[] digest(@NotNull final byte[] input, @NotNull final MessageDigest digest,
                                 @Nullable final byte[] salt, @NotNull final Integer iterations) {
        // 带盐
        if (null != salt) {
            digest.update(salt);
        }

        // 第一次散列
        byte[] result = digest.digest(input);

        // 如果迭代次数>1，进一步迭代散列
        int i;
        for (i = 1; i < iterations; i++) {
            digest.reset();
            result = digest.digest(result);
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取  MessageDigest
     *
     * @param messageDigest ThreadLocal<MessageDigest>
     * @return MessageDigest
     */
    private static MessageDigest getMessageDigest(@NotNull final ThreadLocal<MessageDigest> messageDigest) {
        MessageDigest instance = messageDigest.get();
        instance.reset();
        return instance;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ThreadLocal 创建重用 MessageDigest
     *
     * @param digest String
     * @return ThreadLocal<MessageDigest>
     */
    private static ThreadLocal<MessageDigest> createThreadLocalMessageDigest(@NotNull final String digest) {
        return ThreadLocal.withInitial(() -> {
            try {
                return MessageDigest.getInstance(digest);
            } catch (NoSuchAlgorithmException e) {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>创建MessageDigest加密失败[{}][{}]<<<<<<<<", digest, e.getMessage());
                }
                throw ExceptionUtils.unchecked(e);
            }
        });
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DigestUtils class

/* End of file DigestUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/security/DigestUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
