/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.system.loggingevent;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;


/**
 * 后台logback日志实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminLoggingEventVO implements Serializable {


    private static final long serialVersionUID = 2770188450146308414L;

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "logging_event";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "logging_";
    /**
     *日志ID
     */
    public static final String EVENT_ID = "event_id";
    /**
     *类型
     */
    public static final String LEVEL_STRING = "level_string";
    /**
     *文件名称
     */
    public static final String CALLER_FILENAME = "caller_filename";
    /**
     *日志名称
     */
    public static final String LOGGER_NAME = "logger_name";
    /**
     *线程名称
     */
    public static final String THREAD_NAME = "thread_name";
    /**
     *类名
     */
    public static final String CALLER_CLASS = "caller_class";
    /**
     *方法
     */
    public static final String CALLER_METHOD = "caller_method";
    /**
     *参数
     */
    public static final String ARG0 = "arg0";
    /**
     *参数
     */
    public static final String ARG1 = "arg1";
    /**
     *参数
     */
    public static final String ARG2 = "arg2";
    /**
     *参数
     */
    public static final String ARG3 = "arg3";
    /**
     *日志信息
     */
    public static final String FORMATTED_MESSAGE = "formatted_message";
    /**
     *引用
     */
    public static final String REFERENCE_FLAG = "reference_flag";
    /**
     *代码行
     */
    public static final String CALLER_LINE = "caller_line";
    /**
     *创建时间
     */
    public static final String TIMESTMP = "timestmp";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */

    /**
     * 日志ID
     */
    private BigInteger eventId;
    /**
     * 类型
     */
    private String levelString;
    /**
     * 文件名称
     */
    private String callerFilename;
    /**
     * 日志名称
     */
    private String loggerName;
    /**
     * 线程名称
     */
    private String threadName;
    /**
     * 类名
     */
    private String callerClass;
    /**
     * 方法
     */
    private String callerMethod;
    /**
     * 参数
     */
    private String arg0;
    /**
     * 参数
     */
    private String arg1;
    /**
     * 参数
     */
    private String arg2;
    /**
     * 参数
     */
    private String arg3;
    /**
     * 日志信息
     */
    private String formattedMessage;
    /**
     * 引用
     */
    private String referenceFlag;
    /**
     * 代码行
     */
    private String callerLine;
    /**
     * 创建时间
     */
    private BigInteger timestmp;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminLoggingEventVO class

/* End of file AdminLoggingEventVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/system/loggingevent/AdminLoggingEventVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
