/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.number;


import wang.encoding.mroot.common.annotation.NotNull;

import java.math.BigInteger;

/**
 * BigInteger 工具类
 *
 * @author ErYang
 */
public class BigIntegerUtils {


    /**
     * 大于
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return boolean
     */
    public static boolean gt(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return 1 == BigIntegerUtils.compareTo(var1, var2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 大于等于
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return boolean
     */
    public static boolean gte(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return 0 <= BigIntegerUtils.compareTo(var1, var2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 小于
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return boolean
     */
    public static boolean lt(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return -1 == BigIntegerUtils.compareTo(var1, var2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 小于等于
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return boolean
     */
    public static boolean lte(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return 0 >= BigIntegerUtils.compareTo(var1, var2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 等于
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return boolean
     */
    public static boolean et(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return 0 == BigIntegerUtils.compareTo(var1, var2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 比较大小
     * @param var1 BigInteger
     * @param var2 BigInteger
     * @return int
     */
    private static int compareTo(@NotNull final BigInteger var1, @NotNull final BigInteger var2) {
        return var1.compareTo(var2);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BigIntegerUtils class

/* End of file BigIntegerUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/number/BigIntegerUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
