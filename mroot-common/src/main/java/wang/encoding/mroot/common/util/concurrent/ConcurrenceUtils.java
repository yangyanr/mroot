package wang.encoding.mroot.common.util.concurrent;

import com.google.common.util.concurrent.RateLimiter;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.concurrent.limiter.RateLimiterUtils;
import wang.encoding.mroot.common.util.concurrent.limiter.SamplerUtils;
import wang.encoding.mroot.common.util.concurrent.limiter.TimeIntervalLimiterUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * 并发常用工具类
 *
 * @author ErYang
 */
public class ConcurrenceUtils {

    /**
     * 返回没有激烈CAS冲突的LongAdder, 并发的＋1将在不同的Counter里进行，只在取值时将多个Counter求和.
     *
     * 为了保持JDK版本兼容性，统一采用移植版
     *
     * @return LongAdder
     */
    public static LongAdder longAdder() {
        return new LongAdder();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回CountDownLatch, 每条线程减1，减到0时正在latch.wait()的进程继续进行
     *
     * @param count int
     * @return CountDownLatch
     */
    public static CountDownLatch countDownLatch(final int count) {
        return new CountDownLatch(count);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回CyclicBarrier，每条线程减1并等待，减到0时，所有线程继续运行
     *
     * @param count int
     * @return CyclicBarrier
     */
    public static CyclicBarrier cyclicBarrier(final int count) {
        return new CyclicBarrier(count);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回默认的非公平信号量，先请求的线程不一定先拿到信号量
     *
     * @param permits int
     * @return Semaphore
     */
    public static Semaphore nonFairSemaphore(final int permits) {
        return new Semaphore(permits);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回公平的信号量，先请求的线程先拿到信号量
     *
     * @param permits int
     * @return Semaphore
     */
    public static Semaphore fairSemaphore(final int permits) {
        return new Semaphore(permits, true);
    }

    // -------------------------------------------------------------------------------------------------

    /////////// 限流采样 //////

    /**
     * 返回令牌桶算法的RateLimiter默认版，默认令牌桶大小等于期望的QPS，且刚启动时桶为空。
     *
     * @param  permitsPerSecond int 每秒允许的请求数，可看成QPS，同时将QPS平滑到毫秒级别上，请求到达速度不平滑时依赖缓冲能力
     *
     * @return RateLimiter
     */
    public static RateLimiter rateLimiter(final int permitsPerSecond) {
        return RateLimiter.create(permitsPerSecond);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回令牌桶算法的RateLimiter定制版，可定制令牌桶的大小，且刚启动时桶已装满。
     *
     * @param permitsPerSecond int 每秒允许的请求数，可看成QPS，同时将QPS平滑到毫秒级别上，请求到达速度不平滑时依赖缓冲能力.
     * @param maxBurstSeconds int 可看成桶的容量，Guava中最大的突发流量缓冲时间，默认是1s, permitsPerSecond * maxBurstSeconds，就是闲时能累积的缓冲token最大数量。
     *
     * @return RateLimiter
     * @throws ReflectiveOperationException
     */
    public static RateLimiter rateLimiter(final int permitsPerSecond, final int maxBurstSeconds)
            throws ReflectiveOperationException {
        return RateLimiterUtils.create(permitsPerSecond, maxBurstSeconds);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回采样器
     *
     * @param selectPercent double 采样率，在0-100 之间，可以有小数位
     *
     * @return Sampler
     */
    public static SamplerUtils sampler(final double selectPercent) {
        return SamplerUtils.create(selectPercent);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回时间间隔限制器.
     * @param interval long 间隔时间
     * @param timeUnit TimeUnit 间隔时间单位
     *
     * @return TimeIntervalLimiter
     */
    public static TimeIntervalLimiterUtils timeIntervalLimiter(final long interval, @NotNull final TimeUnit timeUnit) {
        return new TimeIntervalLimiterUtils(interval, timeUnit);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConcurrenceUtils class

/* End of file ConcurrenceUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/ConcurrenceUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
