/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.controller.index;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.controller.BlogBaseController;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.service.blog.cms.BlogArticleService;
import wang.encoding.mroot.vo.blog.entity.cms.article.BlogArticleGetVO;

import java.util.List;

/**
 * 首页控制器
 *
 * @author ErYang
 */
@RestController
public class IndexController extends BlogBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/index";
    /**
     * 首页
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = "/";
    private static final String INDEX_VIEW = MODULE_NAME + INDEX;


    private final BlogArticleService articleService;

    public IndexController(BlogArticleService articleService) {
        this.articleService = articleService;
    }


    /**
     * 主页页面
     *
     * @return ModelAndView
     */
    @RequestMapping(INDEX_URL)
    public ModelAndView index() throws Exception {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW);
        BlogArticleGetVO articleGetVO = new BlogArticleGetVO();
        articleGetVO.setState(StateEnum.NORMAL.getKey());
        articleGetVO.setDisplay(BooleanEnum.YES.getKey());

        Page<BlogArticleGetVO> pageInt = new Page<>();
        IPage<BlogArticleGetVO> page = articleService
                .list2page(super.initPage(pageInt), articleGetVO, BlogArticleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, page);

        // 最热文章
        BlogArticleGetVO hotArticleGetVO = new BlogArticleGetVO();
        hotArticleGetVO.setState(StateEnum.NORMAL.getKey());
        hotArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
        List<BlogArticleGetVO> hotList = articleService.list(hotArticleGetVO, 12, BlogArticleGetVO.PAGE_VIEW, false);
        modelAndView.addObject("hotList", hotList);

        modelAndView.addObject(NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// End IndexController class

/* End of file IndexController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/controller/index/IndexController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
