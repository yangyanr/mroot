/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.bo.admin.entity.system.user;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * 后台用户业务类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminUserBO implements Serializable {


    private static final long serialVersionUID = -757895979062526480L;


    /**
     * ID
     */
    private BigInteger id;
    /**
     * 用户类型(1是管理员，2是普通用户)
     */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 10, message = "validation.system.user.type.range")
    private Integer category;
    /**
     * 用户名
     * @TableField(value = "username", condition = SqlCondition.LIKE_RIGHT)
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{5,18}$", message = "validation.system.user.username.pattern")
    private String username;
    /**
     * 昵称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{0,18}$", message = "validation.system.user.nickName.pattern")
    private String nickName;
    /**
     * 密码
     */
    @Pattern(regexp = "^[0-9a-zA-Z=]{6,128}$", message = "validation.system.user.password.pattern")
    private String password;
    /**
     * 电子邮箱
     */
    @Pattern(regexp = "^([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)){0,128}$", message = "validation.system.user.email.pattern")
    private String email;
    /**
     * 手机号码
     */
    @Pattern(regexp = "^(1[3,5,8][0-9][0-9]{8}){0,32}$", message = "validation.system.user.phone.pattern")
    private String phone;
    /**
     * 真实姓名
     */
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{0,96}$", message = "validation.system.user.realName.pattern")
    private String realName;
    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    private Date gmtCreate;
    /**
     * 创建 IP
     */
    @NotNull(message = "validation.gmtCreateIp.pattern")
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    private Date gmtModified;
    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
    private Integer state;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminUserBO class

/* End of file AdminUserBO.java */
/* Location; ./src/main/java/wang/encoding/mroot/bo/admin/entity/system/user/AdminUserBO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
