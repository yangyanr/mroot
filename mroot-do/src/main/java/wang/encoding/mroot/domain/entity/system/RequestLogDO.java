/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.system;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台请求日志实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("system_request_log")
public class RequestLogDO extends Model<RequestLogDO> implements Serializable {


    private static final long serialVersionUID = -8599325661449481918L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;
    /**
     * 模块
     */
    private String model;
    /**
     * 用户ID
     */
    private BigInteger userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 客户端
     */
    private String userAgent;
    /**
     * 描述
     */
    private String title;
    /**
     * 类名称
     */
    private String className;
    /**
     * 方法名称
     */
    private String methodName;
    /**
     * session名称
     */
    private String sessionName;
    /**
     * 请求地址
     */
    private String url;
    /**
     * 请求的方法类型
     */
    private String methodType;
    /**
     * 请求参数
     */
    private String params;
    /**
     * 返回结果
     */
    private String result;
    /**
     * 执行时间,单位:毫秒
     */
    private Long executeTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogDO class

/* End of file RequestLogDO.java */
/* Location: ./src/main/java/wang/encoding/mroot/domain/entity/system/RequestLogDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
