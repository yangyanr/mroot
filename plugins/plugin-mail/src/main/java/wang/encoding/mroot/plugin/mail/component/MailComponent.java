/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.mail.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.plugin.mail.constant.MailConst;

import javax.annotation.PostConstruct;

/**
 * 邮箱工具类
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class MailComponent {


    private static MailComponent mailComponent;


    private final MailConst mailConst;


    /**
     *Spring 提供的邮件发送类
     */
    private final JavaMailSender javaMailSender;

    @Autowired(required = false)
    @Lazy
    public MailComponent(JavaMailSender javaMailSender, MailConst mailConst) {
        this.javaMailSender = javaMailSender;
        this.mailConst = mailConst;
    }

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[MailComponent]<<<<<<<<");
        }
        mailComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 发送简单邮箱
     * @param toMail String 接收邮箱
     * @param title String 标题
     * @param content String 内容
     * @return boolean true(成功)/false(失败)
     */
    public static boolean sendSimpleEmail(@NotNull final String toMail, @NotNull final String title,
            @NotNull final String content) {
        // 创建简单邮件消息
        SimpleMailMessage message = new SimpleMailMessage();
        // 设置发送人
        message.setFrom(mailComponent.mailConst.getFromMailAddress());
        // 设置收件人
        message.setTo(toMail);
        // 设置主题
        message.setSubject(title);
        // 设置内容
        message.setText(content);
        boolean flag;
        try {
            flag = true;
            // 执行发送邮件
            mailComponent.javaMailSender.send(message);
        } catch (Exception e) {
            flag = false;
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>发送简单邮件出现异常,[接收邮箱:{},发标标题:{}]>>>>>>>>", toMail, title, e);
            }
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MailComponent class

/* End of file MailComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/mail/component/MailComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
