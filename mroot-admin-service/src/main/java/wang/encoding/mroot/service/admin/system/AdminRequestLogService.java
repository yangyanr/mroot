/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.bo.admin.entity.system.requestlog.AdminRequestLogBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.requestlog.AdminRequestLogGetVO;

import java.math.BigInteger;


/**
 * 后台 请求日志 Service 接口
 *
 * @author ErYang
 */
public interface AdminRequestLogService {

    /**
     * 得到所有数目
     *
     * @return int
     */
    int getCount();

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminRequestLogGetVO
     */
    AdminRequestLogGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 请求日志
     *
     * @param adminRequestLogBO AdminRequestLogBO
     * @return boolean
     */
    boolean save(@NotNull final AdminRequestLogBO adminRequestLogBO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 请求日志
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean deleteById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean deleteBatch(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空记录
     *
     * @return boolean
     */
    boolean truncate();

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRequestLogGetVO>
     * @param adminRequestLogGetVO AdminRequestLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRequestLogGetVO>
     */
    IPage<AdminRequestLogGetVO> list2page(@NotNull final Page<AdminRequestLogGetVO> pageAdmin,
            @NotNull final AdminRequestLogGetVO adminRequestLogGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size int
     * @return boolean
     */
    boolean delete2QuartzJob(int size);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogService interface

/* End of file RequestLogService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/RequestLog.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
