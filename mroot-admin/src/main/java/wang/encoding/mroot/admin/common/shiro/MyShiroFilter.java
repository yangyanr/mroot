/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.bo.admin.entity.system.rule.AdminRuleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.ObjectUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.service.admin.system.AdminUserService;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.TreeSet;

/**
 * shiro 访问控制过滤器
 *
 * @author ErYang
 */
@Slf4j
@Component
public class MyShiroFilter extends AccessControlFilter {

    /**
     * 错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";
    /**
     * SOFABoot 版本信息汇总
     */
    private static final String ACTUATOR_VERSIONS_URL = "/actuator/versions";
    /**
     * SOFABoot 应用 Readiness Check 的状况
     */
    private static final String ACTUATOR_READINESS_URL = "/actuator/readiness";

    private final ConfigConst configConst;
    private final AdminUserService adminUserService;
    private final AdminRuleService adminRuleService;
    private final AdminRoleService adminRoleService;
    @Value("${server.servlet.contextPath}")
    private String contextPath;

    @Autowired
    @Lazy
    public MyShiroFilter(ConfigConst configConst, AdminUserService adminUserService, AdminRuleService adminRuleService,
            AdminRoleService adminRoleService) {
        this.configConst = configConst;
        this.adminUserService = adminUserService;
        this.adminRuleService = adminRuleService;
        this.adminRoleService = adminRoleService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 类似于 AOP 中的前置增强；在拦截器链执行之前执行；
     * 如果返回true 则继续拦截器链；否则中断后续的拦截器链的执行直接返回；
     * 进行预处理（如基于表单的身份验证、授权）
     *
     * @param servletRequest  ServletRequest
     * @param servletResponse ServletResponse
     *
     * @return true/false
     * @throws IOException
     */
    @Override
    public boolean preHandle(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {
        Subject subject = getSubject(servletRequest, servletResponse);
        if (null == subject) {
            return false;
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String url = request.getRequestURI();
        // 不拦截的 url
        boolean flag = this.filtration(url);
        if (flag) {
            return true;
        }

        // 得到登录的用户
        AdminUserGetVO onlineUserVO = (AdminUserGetVO) ShiroSessionUtils
                .getAttribute(configConst.getAdminSessionName());
        // 得到用户的权限菜单
        List menu = (List) ShiroSessionUtils.getAttribute(configConst.getAdminMenuName());
        if (null == onlineUserVO || null == menu) {
            String username = (String) subject.getPrincipal();
            if (StringUtils.isBlank(username)) {
                // 跳转到后台登录页面
                saveRequestAndRedirectToLogin(servletRequest, servletResponse);
                return false;
            }
            AdminUserGetVO userVO = adminUserService.getByUsername(username);
            if (null == userVO || StateEnum.DISABLE.getKey() == userVO.getState()) {
                // 跳转到后台登录页面
                saveRequestAndRedirectToLogin(servletRequest, servletResponse);
                return false;
            }
            // 把用户的权限菜单放入 session 中
            boolean rulesPutSessionFlag = rulesPutSession(userVO);
            if (!rulesPutSessionFlag) {
                saveRequestAndRedirectToLogin(servletRequest, servletResponse);
                return false;
            }
        }

        // 权限校验 判断是否包含权限
        // 具体响应 ShiroDbRealm 中 doGetAuthorizationInfo 方法 判断是否包含此 url 的响应权限
        boolean isPermitted = subject.isPermitted(url);
        if (isPermitted) {
            return true;
        } else {
            // 跳转到无权限异常页面
            ShiroSessionUtils.removeAttribute(configConst.getAdminSessionName());
            ShiroSessionUtils.removeAttribute(configConst.getAdminMenuName());
            subject.logout();
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect(contextPath + configConst.getAdminUnauthorizedUrlName());
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 表示是否允许访问；o 就是 [urls] 配置中拦截器参数部分，
     * 如果允许访问返回true，否则false；
     *
     * @param servletRequest  ServletRequest
     * @param servletResponse ServletResponse
     * @param o               Object
     * @return boolean
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) {
        Subject subject = getSubject(servletRequest, servletResponse);
        return null != subject.getPrincipal();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * onAccessDenied：表示当访问拒绝时是否已经处理了；如果返回 true 表示需要继续处理
     * 如果返回 false 表示该拦截器实例已经处理了，将直接返回即可
     *
     * @param servletRequest  servletRequest
     * @param servletResponse servletResponse
     * @return boolean
     */
    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) {
        return false;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 不拦截的 url
     * @param url String
     * @return boolean
     */
    private boolean filtration(@NotNull final String url) {
        // 根目录
        String bias = "/";
        if (url.equals(bias)) {
            return true;
        }
        // 后台首页页面
        if (url.startsWith(contextPath + configConst.getAdminIndexUrlName())) {
            return true;
        }
        // 登录页面
        if (url.startsWith(contextPath + configConst.getAdminLoginUrlName())) {
            return true;
        }
        // 处理登录
        if (url.startsWith(contextPath + configConst.getAdminDoLoginUrlName())) {
            return true;
        }
        // 退出页面
        if (url.startsWith(contextPath + configConst.getAdminLogoutUrlName())) {
            return true;
        }
        // 验证码
        if (url.startsWith(contextPath + configConst.getAdminVerifyImageUrlName())) {
            return true;
        }
        // 资源文件
        if (url.startsWith(contextPath + configConst.getAdminStaticPathUrlName())) {
            return true;
        }
        // 无权限页面
        if (url.startsWith(contextPath + configConst.getAdminUnauthorizedUrlName())) {
            return true;
        }
        // 改变语言
        if (url.startsWith(contextPath + configConst.getAdminLanguageUrlName())) {
            return true;
        }
        // SOFABoot 版本信息汇总
        if (url.startsWith(contextPath + ACTUATOR_VERSIONS_URL)) {
            return true;
        }
        // SOFABoot 应用 Readiness Check 的状况
        if (url.startsWith(contextPath + ACTUATOR_READINESS_URL)) {
            return true;
        }
        // 错误页面
        return url.startsWith(contextPath + ERROR404_URL);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 把用户的权限菜单放入 session 中
     * @param userVO AdminUserGetVO
     * @return boolean
     */
    private boolean rulesPutSession(@NotNull final AdminUserGetVO userVO) {
        // 权限
        List<AdminRuleBO> rulesVO = ListUtils.newArrayList();
        // 所属角色
        AdminRoleBO rolesVO = adminRoleService.getByUserId(userVO.getId());
        if (null != rolesVO) {
            TreeSet<AdminRuleBO> ruleVOList = adminRuleService.listByRoleId(rolesVO.getId());
            if (CollectionUtils.isNotEmpty(ruleVOList)) {
                for (AdminRuleBO ruleVO : ruleVOList) {
                    if (!ObjectUtils.equals(BigInteger.ZERO, ruleVO.getPid())) {
                        rulesVO.add(ruleVO);
                    }
                }
            }
            if (ListUtils.isNotEmpty(rulesVO)) {
                // list 转为 tree
                List<AdminRuleBO> tree = AdminRuleBO.list2Tree(rulesVO);
                if (null != tree) {
                    // 用户权限菜单存放在 session 中
                    ShiroSessionUtils.setAttribute(configConst.getAdminMenuName(), tree);
                    return true;
                }
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MyShiroFilter class

/* End of file MyShiroFilter.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/MyShiroFilter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
