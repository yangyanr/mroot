/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.generate;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.model.GenerateModel;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.FreemarkerUtils;
import wang.encoding.mroot.common.util.io.FileUtils;
import wang.encoding.mroot.common.util.io.PathUtils;
import wang.encoding.mroot.service.admin.system.AdminConfigService;

import java.io.File;
import java.util.*;

/**
 * 代码生成控制器
 *
 * @author ErYang
 */
@RestController
@RequestMapping("/system/generatecode")
public class GenerateCodeController extends AdminBaseController {

    /**
     *  table 标识名称
     */
    private static final String TABLE_NAME = "table";
    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/generatecode";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/generatecode";
    /**
     * 首页
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 生成
     */
    private static final String GENERATE = "/generate";
    private static final String GENERATE_NAME = "generate";
    private static final String GENERATE_URL = MODULE_NAME + GENERATE;
    private static final String GENERATE_VIEW = VIEW_PATH + GENERATE;

    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;

    /**
     *属性集合
     */
    private List<String[]> fieldList = new ArrayList<>();
    /**
     *创建数据模型
     */
    private Map<String, Object> root = new HashMap<>(16);
    /**
     *数据库id
     */
    private static final String DATA_ID = "id";
    /**
     *数据库id 类型
     */
    private String dataIdType = null;
    /**
     *数据库id 类型 大写
     */
    private String dataIdTypeUpperCase = null;
    private int mapStartIndex = 0;
    private static final String TABLE_VAR = "表";
    private static final String LINE_VAR = "_";
    private static final String SPLIT_NAME = "#";
    /**
     *存放路径
     */
    private static final String FILE_PATH = "/generate/code/";


    private final AdminConfigService configService;

    @Autowired
    public GenerateCodeController(AdminConfigService configService) {
        this.configService = configService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_GENERATE_VIEW)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        String table = super.request.getParameter("table");
        List<String> tableArray = new ArrayList<>();
        if (StringUtils.isNotBlank(table)) {
            tableArray.add(table.trim());
            modelAndView.addObject("table", table.trim());
        }
        Page<Map<String, String>> pageInt = new Page<>();
        IPage<Map<String, String>> page = configService.listTableByMap(super.initPage(pageInt), tableArray);
        modelAndView.addObject("page", page);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(GENERATE_NAME, contextPath + GENERATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成页面
     *
     * @param table String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(GENERATE_URL)
    @RequestMapping(GENERATE + "/{table}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_GENERATE_CODE_VIEW)
    public ModelAndView generate(@PathVariable(TABLE_NAME) String table, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(GENERATE_VIEW, GENERATE_URL);
        // 开发环境
        if (ProfileComponent.isDevProfile()) {
            if (StringUtils.isBlank(table)) {
                super.addFailMessage(redirectAttributes, LocaleMessageSourceComponent.getMessage("message.info.error"));
                modelAndView.setViewName(super.initRedirectUrl(INDEX_URL));
                return modelAndView;
            }
            modelAndView.addObject("table", table);
        } else {
            super.addFailMessage(redirectAttributes,
                    LocaleMessageSourceComponent.getMessage("message.generateCode.error"));
            modelAndView.setViewName(super.initRedirectUrl(INDEX_URL));
            return modelAndView;
        }
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理生成
     *
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_GENERATE_SAVE)
    public Object save() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 不是开发环境
            if (!ProfileComponent.isDevProfile()) {
                return super.initErrorJSONObject(failResult, "message.generateCode.error");
            }

            String table = super.request.getParameter("table");
            if (StringUtils.isBlank(table)) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 是否保留前缀
            String tablePrefix = super.request.getParameter("tablePrefix");

            List<Map<String, String>> tableList = configService.getTableByTableName(table);
            if (null == tableList || tableList.isEmpty()) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 生成
            if (null == tablePrefix) {
                tablePrefix = "";
            }
            // 生成
            this.init(tablePrefix, tableList);
            ResultData resultData = ResultData.ok();
            return super.initSucceedJSONObject(resultData, "message.generate.info.succeed");
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成
     *
     * @param tablePrefixName String 是否保留表前缀
     * @param tableList List<Map<String, String>>  表列表
     */
    private void init(@NotNull final String tablePrefixName, @NotNull final List<Map<String, String>> tableList) {
        // 生成代码前,先清空之前生成的代码
        FileUtils.deleteDir(new File(PathUtils.getClassPath() + "/generate"));
        this.initTable(tablePrefixName, tableList);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成
     * @param tablePrefixName String 是否保留表前缀
     * @param tableList List<Map<String, String>>  表列表
     */
    private void initTable(@NotNull final String tablePrefixName, @NotNull final List<Map<String, String>> tableList) {
        // 生成内容 model
        List<Object> generateModels = new ArrayList<>();
        for (Map<String, String> tableMap : tableList) {
            // 表名
            String tableName = tableMap.get("tableName");
            // 表描述
            String tableComment = tableMap.get("tableComment");
            // 表前缀
            String tablePrefix;
            if (tableName.contains(LINE_VAR)) {
                tablePrefix = tableName.substring(0, tableName.indexOf(LINE_VAR));
            } else {
                tablePrefix = "";
            }
            // 实体类描述
            // 移除带表名的字
            String classComment;
            if (tableComment.endsWith(TABLE_VAR)) {
                classComment = tableComment.substring(0, tableComment.lastIndexOf(TABLE_VAR));
            } else {
                classComment = tableComment;
            }
            // 类名 保留前缀
            String className;
            if (configConst.getBootstrapSwitchEnabled().equals(tablePrefixName)) {
                className = wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(tableName);
            } else {
                if (StringUtils.isNotBlank(tablePrefix)) {
                    className = wang.encoding.mroot.common.util.text.StringUtils
                            .toCamelCase(tableName.substring(tablePrefix.length() + 1));
                } else {
                    className = wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(tableName);
                }
            }
            // 类名 首字母大写
            className = StringUtils.capitalize(className);
            // 类前缀 首字母小写
            String classPrefix = StringUtils.uncapitalize(tablePrefix);
            // 表详情
            List<Map<String, String>> tableDetailList = configService.getTableDetailByTableName(tableName);
            for (Map<String, String> map : tableDetailList) {
                // 字段备注
                String defaultVal;
                if (StringUtils.isNotBlank(map.get("columnComment"))) {
                    defaultVal = map.get("columnComment");
                } else {
                    defaultVal = " ";
                }
                // 数据库中的数据类型
                String dataType = map.get("dataType");
                // 数据库中的数据类型 大写
                String dataTypeUpperCase = this.initDataTypeUpperCase(dataType);
                // 数据库数据类型转为 mybatis 类型
                dataType = this.initDataType(dataType);
                // 表id
                if (DATA_ID.equals(map.get("columnName"))) {
                    dataIdType = dataType;
                    dataIdTypeUpperCase = dataTypeUpperCase;
                }
                // 字段备注
                String columnName = map.get("columnName");
                // 字段组合
                String filed = (columnName + SPLIT_NAME + dataType + SPLIT_NAME + dataTypeUpperCase + SPLIT_NAME
                        + defaultVal + SPLIT_NAME + wang.encoding.mroot.common.util.text.StringUtils
                        .toCamelCase(columnName));
                // 属性放到集合里面
                fieldList.add(filed.split(SPLIT_NAME));
                // 初始化 GenerateModel
                GenerateModel generateModel = this.initGenerateModel(fieldList.get(mapStartIndex));
                generateModels.add(generateModel);
                mapStartIndex++;
            }
            // 初始化节点数据
            this.initRoot(className, classPrefix, tableName, tablePrefix, tableComment, classComment, dataIdType,
                    dataIdTypeUpperCase, generateModels, fieldList, root);
            // 初始化 freemarker 数据
            initFreemarker(className, classPrefix, root);
        }
    }

    /**
     * 数据库字段类型转为大写
     *
     *  @param dataType String
     * @return String
     */
    private String initDataTypeUpperCase(@NotNull final String dataType) {
        String intName = "int";
        String integerName = "integer";
        String tinyintName = "tinyint";
        String bigintName = "bigint";
        String textName = "text";
        String datetimeName = "datetime";
        String timestampName = "timestamp";
        if (intName.equalsIgnoreCase(dataType) || integerName.equalsIgnoreCase(dataType)) {
            return "INTEGER";
        } else if (tinyintName.equalsIgnoreCase(dataType)) {
            return "TINYINT";
        } else if (bigintName.equalsIgnoreCase(dataType)) {
            return "BIGINT";
        } else if (textName.equalsIgnoreCase(dataType)) {
            return "VARCHAR";
        } else if (datetimeName.equalsIgnoreCase(dataType) || timestampName.equalsIgnoreCase(dataType)) {
            return "TIMESTAMP";
        } else {
            return dataType.toUpperCase();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 数据库字段类型转为 Java 类型
     *
     *  @param dataType String
     * @return String
     */
    private String initDataType(@NotNull final String dataType) {
        String intName = "int";
        String integerName = "integer";
        String tinyintName = "tinyint";
        String bigintName = "bigint";
        String datetimeName = "datetime";
        String timestampName = "timestamp";
        String dateName = "date";
        String decimalName = "decimal";
        String textName = "text";
        String varcharName = "varchar";

        String dataTypeUpperCase;
        if (intName.equalsIgnoreCase(dataType) || integerName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "Integer";
        } else if (tinyintName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "Integer";
        } else if (bigintName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "BigInteger";
        } else if (datetimeName.equalsIgnoreCase(dataType) || timestampName.equals(dataType)) {
            dataTypeUpperCase = "Date";
        } else if (dateName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "Date";
        } else if (decimalName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "BigDecimal";
        } else if (textName.equalsIgnoreCase(dataType) || varcharName.equalsIgnoreCase(dataType)) {
            dataTypeUpperCase = "String";
        } else {
            dataTypeUpperCase = "String";
        }
        return dataTypeUpperCase;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 GenerateModel
     *
     *  @param fieldList String[] 字段数组
     *
     * @return GenerateModel
     */
    private GenerateModel initGenerateModel(@NotNull final String[] fieldList) {
        // 移除类前缀驼峰命名
        String removeClassPrefixHump = wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(fieldList[0]);
        // 移除类前缀驼峰命名 首字母小写
        String removeClassPrefixFirstLowerCaseHump = StringUtils.uncapitalize(removeClassPrefixHump);
        // 移除类前缀驼峰命名 首字母大写
        String removeClassPrefixFirstUpperCaseHump = StringUtils.capitalize(removeClassPrefixHump);
        // GenerateModel
        GenerateModel generateModel = new GenerateModel();
        generateModel.setName(fieldList[0]);
        generateModel.setType(fieldList[1]);
        generateModel.setDataType(fieldList[2]);
        generateModel.setComment(fieldList[3]);
        generateModel.setDefaultValue(fieldList[4]);
        generateModel.setDataId(GenerateCodeController.DATA_ID);
        generateModel.setCamelCaseName(removeClassPrefixHump);
        generateModel.setFirstUnCapitalizeCamelCaseName(removeClassPrefixFirstLowerCaseHump);
        generateModel.setFirstCapitalizeCamelCaseName(removeClassPrefixFirstUpperCaseHump);
        generateModel.setFinalName(fieldList[0].toUpperCase());
        return generateModel;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化节点数据
     *   @param className String 类名称
     *  @param classPrefix String 类前缀
     * @param tableName String 表名称
     * @param tablePrefix String 表前缀
     * @param tableComment String 表描述
     * @param classComment String 类描述
     * @param dataIdType String 表id类型
     * @param dataIdTypeUpperCase String 表id类型大写
     * @param generateModels List<Object> 模型集合
     * @param fieldList List<String[]> 字段集合
     * @param root Map<String, Object> 节点集合
     *
     */
    private void initRoot(@NotNull final String className, @NotNull final String classPrefix,
            @NotNull final String tableName, @NotNull final String tablePrefix, @NotNull final String tableComment,
            @NotNull final String classComment, @NotNull final String dataIdType,
            @NotNull final String dataIdTypeUpperCase, @NotNull final List<Object> generateModels,
            @NotNull final List<String[]> fieldList, @NotNull final Map<String, Object> root) {
        // DO 层包名
        String doPackageName = "wang.encoding.mroot.domain.entity";
        // DAO 层包名
        String daoPackageName = "wang.encoding.mroot.dao";
        // BO 层包名
        String boPackageName = "wang.encoding.mroot.bo.admin.entity";
        // Service层包名
        String servicePackageName = "wang.encoding.mroot.service";
        // Admin Service层包名
        String adminServicePackageName = "wang.encoding.mroot.service.admin";
        // VO层包名
        String voPackageName = "wang.encoding.mroot.vo.admin.entity";
        // Controller层包名
        String controllerPackageName = "wang.encoding.mroot.admin.controller";


        // 创建数据模型
        root.put("fieldList", fieldList);
        root.put("generateModels", generateModels);
        // 类名
        root.put("className", className);
        // 类名小写
        root.put("classNameLowerCase", className.toLowerCase());
        // 类名大写
        root.put("classNameUppercase", className.toUpperCase());
        // 类名首字母小写
        root.put("classFirstLowerCaseName", StringUtils.uncapitalize(className));
        // 当前日期
        root.put("nowDate", new Date());
        // 类前缀
        root.put("classPrefix", classPrefix);
        // 类前缀大写
        root.put("classPrefixUppercase", classPrefix.toUpperCase());
        // 表名称
        root.put("tableName", tableName);
        // 表前缀
        root.put("tablePrefix", tablePrefix);
        // 表描述
        root.put("tableComment", tableComment);
        // model描述
        root.put("classComment", classComment);
        // 表id
        root.put("dataId", GenerateCodeController.DATA_ID);
        if (StringUtils.isBlank(dataIdType)) {
            root.put("dataIdType", "BigInteger");
        } else {
            root.put("dataIdType", dataIdType);
        }
        if (StringUtils.isBlank(dataIdTypeUpperCase)) {
            root.put("dataIdTypeUpperCase", "BIGINT");
        } else {
            root.put("dataIdTypeUpperCase", dataIdTypeUpperCase);
        }
        root.put("doPackageName", doPackageName);
        root.put("daoPackageName", daoPackageName);
        root.put("boPackageName", boPackageName);
        root.put("servicePackageName", servicePackageName);
        root.put("adminServicePackageName", adminServicePackageName);
        root.put("voPackageName", voPackageName);
        root.put("controllerPackageName", controllerPackageName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  初始化 freemarker 数据
     *
     *  @param className String 类名称
     *  @param classPrefix String 类前缀名称
     *  @param root Map<String, Object> 节点数据
     *
     */
    private void initFreemarker(@NotNull final String className, @NotNull final String classPrefix,
            @NotNull final Map<String, Object> root) {
        // 模板路径
        String ftlPath = "/templates/view";
        String path = ftlPath + super.getCurrentThemePath() + "/system/generatecode/template";

        // 目录 文件名 全部小写
        String lowerCaseClassName = className.toLowerCase();
        // 目录 文件名 全部小写
        String lowerCaseClassPrefix = classPrefix.toLowerCase();
        /* 生成 DO */
        FreemarkerUtils.printFile("doTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "do/" + lowerCaseClassName + "/" + className + "DO.java", FILE_PATH, path);
        /* 生成 DAO */
        FreemarkerUtils.printFile("daoTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "dao/" + lowerCaseClassName + "/" + className + "DAO.java", FILE_PATH,
                path);
        /* 生成 mybatis xml */
        FreemarkerUtils.printFile("mapperMybatisTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "mybatis/" + lowerCaseClassName + "/" + className + "Mapper.xml",
                FILE_PATH, path);
        /* 生成 BO */
        FreemarkerUtils.printFile("boTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "bo/" + lowerCaseClassName + "/Admin" + className + "BO.java", FILE_PATH,
                path);
        /* 生成 VO */
        FreemarkerUtils.printFile("voBaseTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "vo/" + lowerCaseClassName + "/Admin" + className + "VO.java", FILE_PATH,
                path);
        FreemarkerUtils.printFile("voGetTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "vo/" + lowerCaseClassName + "/Admin" + className + "GetVO.java",
                FILE_PATH, path);
        /* 生成 Service */
        FreemarkerUtils.printFile("serviceTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "service/" + className + "Service.java", FILE_PATH, path);
        /* 生成 ServiceImpl */
        FreemarkerUtils.printFile("serviceImplTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "service/" + className + "ServiceImpl.java", FILE_PATH, path);
        /* 生成 Admin Service */
        FreemarkerUtils.printFile("adminServiceTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "service/" + "admin/" + "Admin" + className + "Service.java", FILE_PATH,
                path);
        /* 生成 Admin ServiceImpl */
        FreemarkerUtils.printFile("adminServiceImplTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "service/" + "admin/" + "Admin" + className + "ServiceImpl.java",
                FILE_PATH, path);
        /* 生成 controller */
        FreemarkerUtils.printFile("controllerTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "controller/" + lowerCaseClassName + "/" + className + "Controller.java",
                FILE_PATH, path);

        /* 国际化信息 */
        FreemarkerUtils.printFile("zh_CNTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "il8n/" + lowerCaseClassName + "/" + lowerCaseClassName + ".properties",
                FILE_PATH, path);
        FreemarkerUtils.printFile("zh_CNTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "il8n/" + lowerCaseClassName + "/" + lowerCaseClassName
                        + "_zh_CN.properties", FILE_PATH, path);
        FreemarkerUtils.printFile("en_USTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "il8n/" + lowerCaseClassName + "/" + lowerCaseClassName
                        + "_en_US.properties", FILE_PATH, path);

        /* 生成 Freemarker 页面 */
        FreemarkerUtils.printFile("ftlIndexTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/index.ftl", FILE_PATH, path);
        FreemarkerUtils.printFile("ftlAddTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/add.ftl", FILE_PATH, path);
        FreemarkerUtils.printFile("ftlEditTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/edit.ftl", FILE_PATH, path);
        FreemarkerUtils.printFile("ftlViewTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/view.ftl", FILE_PATH, path);

        FreemarkerUtils.printFile("ftlRecycleBinTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/recyclebin.ftl", FILE_PATH, path);

        /* 生成 js 页面 */
        FreemarkerUtils.printFile("jsTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/" + lowerCaseClassName + "js.ftl",
                FILE_PATH, path);

        /* 生成 ftl readme.md */
        FreemarkerUtils.printFile("readmeFtlTemplate.ftl", root,
                lowerCaseClassPrefix + "/" + "ftl/" + lowerCaseClassName + "/README.md", FILE_PATH, path);
    }

    // -------------------------------------------------------------------------------------------------

}

// End GenerateCodeController class

/* End of file GenerateCodeController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/generate/GenerateCodeController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
