MRoot小小木快速开发平台简介


MRoot是基于Spring Boot2编写的快速开发平台。提供强大的代码生成器，一键生成90%的代码！MRoot的宗旨是降低开发成本，提高开发效率。


技术选型

开发语言：Java

核心框架：Spring Boot2、SOFABoot

数据库连接池：Alibaba Druid、Hikari

持久层框架：MyBatis-Plus

安全框架：Apache Shiro

任务调度：Quartz

缓存框架：Redis

日志管理：SLF4J、Logback

验证框架：Hibernate Validation

模板：Freemarker

其他：FastJSON、Lombok等等

前端框架：Bootstrap



主要功能

数据库：多数据源支持，Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能

持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量，使用Hibernate Validation进行数据验证

MVC：基于Spring Mvc注解，Rest风格Controller，Exception统一管理

任务调度：Quartz, 可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能

国际化：基于Spring的国际化信息

安全框架：Shiro进行权限控制，灵活的权限控制，可控制权限到按钮级别

角色：完善的角色管理及数据权限

缓存：注解缓存数据

线程：自定义线程池、异步任务，获取异步任务的处理结果

安全：完善的XSS防范及脚本过滤，防止表单重复提交

日志：logback打印日志，存入数据库，同时基于时间和文件大小分割日志文件

工具类：加密解密、字符串处理等等

CND：七牛或者本地一键切换

上传：七牛或者本地一键切换

邮箱：发送邮件

监控：后台请求记录，异步任务异常发送邮件通知

代码生成：代码自动生成，可生成90%的业务代码

前端：使用Bootstrap，优美的页面，丰富的插件

二次开发：友好的代码结构及注释，便于阅读及二次开发


开发环境

语言：Java 8

IDE：IDEA

依赖管理：Maven 3.5.2+

数据库：MySQL 8+ & MariaDB 10.3+ 


功能模块

用户管理

角色管理：配置角色所拥有的权限

菜单管理：配置角色所拥有的权限

操作日志：系统操作日志记录和查询

代码生成

缓存管理：Redis

分类管理

文章管理

百度编辑器

配置管理

邮箱测试



在线体验

后台管理传送门： http://www.yya.ink/

博客传送门：  http://blog.yya.ink/

