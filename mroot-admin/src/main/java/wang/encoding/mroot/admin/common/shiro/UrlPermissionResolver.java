/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.PermissionResolver;
import org.apache.shiro.authz.permission.WildcardPermission;


/**
 * 自定义权限匹配器
 *
 * @author ErYang
 */
@Slf4j
public class UrlPermissionResolver implements PermissionResolver {

    /**
     * subject.isPermitted(url) 中传入的字符串
     * 和自定义 Realm 中传入的权限字符串集合都要经过这个 resolver
     * @param s String
     * @return Permission
     */
    @Override
    public Permission resolvePermission(String s) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>需要匹配的url[{}]<<<<<<<<", s);
        }
        String index = "/";
        if (s.startsWith(index)) {
            return new UrlPermission(s);
        }
        return new WildcardPermission(s);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UrlPermissionResolver class

/* End of file UrlPermissionResolver.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/UrlPermissionResolver.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
