/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 数据库的配置变量名称
 *
 * @author ErYang
 */
@Component
@PropertySource("classpath:database.properties")
@ConfigurationProperties(prefix = "database")
@EnableCaching
@Getter
@Setter
public class DatabaseConst {

    /**
     * 后台分页条数 名称
     */
    private String adminPageSizeName;
    /**
     * 七牛 cdn 地址 名称
     */
    private String qiniuCdnName;
    /**
     * 七牛 上传名称
     */
    private String qiniuUploadName;
    /**
     * mail 名称
     */
    private String mailName;
    /* 博客 */
    /**
     * 博客分页显示条数
     */
    private String blogPageSizeName;
    /**
     * 博客首页 keywords
     */
    private String blogIndexMetaKeywords;
    /**
     * 博客首页 description
     */
    private String blogIndexMetaDescription;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DatabaseConst class

/* End of file DatabaseConst.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/constant/DatabaseConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
