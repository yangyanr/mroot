<@OVERRIDE name="MAIN_CONTENT">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__body m-portlet__body--no-padding">
    <div class="m-invoice-1">
    <div class="m-invoice__wrapper">
    <div class="m-invoice__head"
    style="background-image: url(${GLOBAL_RESOURCE_MAP['APP']}/media/img/bg/bg-6.jpg);">
    <div class="m-invoice__container m-invoice__container--centered">
    <div class="m-invoice__logo">
    <a href="javascript:">
    <h1>
    <@defaultStr article.title></@defaultStr>
    </h1>
    </a>
<a href="javascript:">
</a>
    </div>
    <span class="m-invoice__desc">
    <span><@defaultStr article.description></@defaultStr></span>
    </span>
    <div class="m-invoice__items">
    <div class="m-invoice__item">
    <span class="m-invoice__subtitle">
    ${I18N("message.cms.blog.view.category")}
    </span>
    <span class="m-invoice__text">
    <@defaultStr article.category.title></@defaultStr>
    </span>
    </div>
    <div class="m-invoice__item">
    <span class="m-invoice__subtitle">
    ${I18N("message.cms.blog.view.pageView")}
    </span>
    <span class="m-invoice__text">
    <@defaultStr article.pageView></@defaultStr>
    </span>
    </div>
    <div class="m-invoice__item">
    <span class="m-invoice__subtitle">
    ${I18N("message.cms.blog.view.datetime")}
    </span>
    <span class="m-invoice__text">
    <@dateFormat  article.gmtCreate></@dateFormat>
    </span>
    </div>
    </div>
    </div>
    </div>
    <div class="m-invoice__body">
    <div class="m--padding-bottom-50">
    <@defaultStr articleContent.content></@defaultStr>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
