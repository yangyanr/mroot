package wang.encoding.mroot.common.util.concurrent.threadpool;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *Queueable缓存ThreadPool
 *
 * @author ErYang
 */
public class QueueableCachedThreadPoolUtils extends java.util.concurrent.ThreadPoolExecutor {


    /**
     * The number of tasks submitted but not yet finished. This includes tasks in the queue and tasks that have been
     * handed to a worker thread but the latter did not start executing the task yet. This number is always greater or
     * equal to {@link #getActiveCount()}.
     */
    private final AtomicInteger submittedCount = new AtomicInteger(0);

    // -------------------------------------------------------------------------------------------------

    public QueueableCachedThreadPoolUtils(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
            ControllableQueue workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        workQueue.setParent(this);
        prestartAllCoreThreads(); // NOSOANR
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        submittedCount.decrementAndGet();
    }

    // -------------------------------------------------------------------------------------------------

    public int getSubmittedCount() {
        return submittedCount.get();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *
     * @param command Runnable
     */
    @Override
    public void execute(Runnable command) {
        execute(command, 0, TimeUnit.MILLISECONDS);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Executes the given command at some time in the future. The command may execute in a new thread, in a pooled
     * thread, or in the calling thread, at the discretion of the <tt>Executor</tt> implementation. If no threads are
     * available, it will be added to the work queue. If the work queue is full, the system will wait for the specified
     * time and it throw a RejectedExecutionException if the queue is still full after that.
     *
     * @param command the runnable task
     * @param timeout A timeout for the completion of the task
     * @param unit The timeout time unit
     * @throws RejectedExecutionException if this task cannot be accepted for execution - the queue is full
     * @throws NullPointerException if command or unit is null
     */
    public void execute(Runnable command, long timeout, TimeUnit unit) {
        submittedCount.incrementAndGet();
        try {
            super.execute(command);
        } catch (RejectedExecutionException rx) {
            final ControllableQueue queue = (ControllableQueue) super.getQueue();
            try {
                if (!queue.force(command, timeout, unit)) {
                    submittedCount.decrementAndGet();
                    throw new RejectedExecutionException(">>>>>>>>队列容量已满<<<<<<<<");
                }
            } catch (InterruptedException ignore) {
                submittedCount.decrementAndGet();
                throw new RejectedExecutionException(ignore);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * https://github.com/apache/tomcat/blob/trunk/java/org/apache/tomcat/util/threads/TaskQueue.java
     */
    protected static class ControllableQueue extends LinkedBlockingQueue<Runnable> {

        private static final long serialVersionUID = 5044057462066661171L;
        private transient volatile QueueableCachedThreadPoolUtils parent = null;

        public ControllableQueue(int capacity) {
            super(capacity);
        }

        // -------------------------------------------------------------------------------------------------

        public void setParent(QueueableCachedThreadPoolUtils tp) {
            parent = tp;
        }

        // -------------------------------------------------------------------------------------------------

        public boolean force(Runnable o) {
            if (parent.isShutdown()) {
                throw new RejectedExecutionException(">>>>>>>>执行程序不运行,无法强制命令进入队列<<<<<<<<");
            }
            return super.offer(o);
        }

        // -------------------------------------------------------------------------------------------------

        public boolean force(Runnable o, long timeout, TimeUnit unit) throws InterruptedException {
            if (parent.isShutdown()) {
                throw new RejectedExecutionException(">>>>>>>>执行程序不运行,无法强制命令进入队列<<<<<<<<");
            }
            return super.offer(o, timeout, unit);
        }

        // -------------------------------------------------------------------------------------------------

        @Override
        public boolean offer(Runnable o) {
            // springtime: threadPool.getPoolSize() 是个有锁的操作，所以尽量减少

            int currentPoolSize = parent.getPoolSize();

            // 我们在线程上已经用完了 只需对对象排队即可
            if (currentPoolSize >= parent.getMaximumPoolSize()) {
                return super.offer(o);
            }
            // 我们有空闲线程 只需将其添加到队列中
            if (parent.getSubmittedCount() < currentPoolSize) {
                return super.offer(o);
            }
            // 如果线程数少于新线程的最大创建力
            if (currentPoolSize < parent.getMaximumPoolSize()) {
                return false;
            }
            // 如果到达这里 我们需要将它添加到队列中
            return super.offer(o);
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QueueableCachedThreadPoolUtils class

/* End of file QueueableCachedThreadPoolUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/threadpool/QueueableCachedThreadPoolUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
