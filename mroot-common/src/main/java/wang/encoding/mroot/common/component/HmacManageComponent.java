/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.constant.SecurityConst;
import wang.encoding.mroot.common.util.CharsetsUtils;
import wang.encoding.mroot.common.util.text.EncodeUtils;
import wang.encoding.mroot.common.util.ExceptionUtils;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * MAC 消息签名 加密
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class HmacManageComponent {


    private static HmacManageComponent hmacManageComponent;


    /**
     * HMAC-MD5
     */
    private static final String HMAC_MD5 = "HmacMD5";
    /**
     * HMAC-SHA1
     */
    private static final String HMAC_SHA1 = "HmacSHA1";
    /**
     * HMAC-SHA256
     */
    private static final String HMAC_SHA256 = "HmacSHA256";
    /**
     * HMAC-SHA512
     */
    private static final String HMAC_SHA512 = "HmacSHA512";

    // -------------------------------------------------------------------------------------------------

    private final SecurityConst securityConst;

    @Autowired
    public HmacManageComponent(SecurityConst securityConst) {
        this.securityConst = securityConst;
    }

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[HmacManageComponent]<<<<<<<<");
        }
        hmacManageComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /***
     *  HmacMD5 加密
     * @param str String 需要加密的字符串
     * @return String 加密后HEX编码的内容
     */
    public static String getHmacMd5(@NotNull final String str) {
        return HmacManageComponent.getMac(str, HMAC_MD5);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     *  HmacSHA1 加密
     * @param str String 需要加密的字符串
     * @return String 加密后HEX编码的内容
     */
    public static String getHmacSha1(@NotNull final String str) {
        return HmacManageComponent.getMac(str, HMAC_SHA1);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     *  HmacSHA256 加密
     * @param str String 需要加密的字符串
     * @return String 加密后HEX编码的内容
     */
    public static String getHmacSha256(@NotNull final String str) {
        return HmacManageComponent.getMac(str, HMAC_SHA256);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     *  HmacSHA512 加密
     * @param str String 需要加密的字符串
     * @return String 加密后HEX编码的内容
     */
    public static String getHmacSha512(@NotNull final String str) {
        return HmacManageComponent.getMac(str, HMAC_SHA512);
    }

    // -------------------------------------------------------------------------------------------------

    /***
     *  HmacSHA512 加密
     * @param str String 需要加密的字符串
     * @return String 加密后HEX编码的内容
     */
    private static String getMac(@NotNull final String str, @NotNull final String type) {
        // 需要编码  编解码方式有 Base64 HEX
        return EncodeUtils.encodeHex(HmacManageComponent.mac(str, type));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 实现 mac 加密
     *
     * @param str  String 需要加密的字符串
     * @param type String 加密类型
     * @return byte[] 加密后的报文
     */
    private static byte[] mac(@NotNull final String str, @NotNull final String type) {
        try {
            SecretKey secretKey = new SecretKeySpec(hmacManageComponent.securityConst.
                    getMacKey().getBytes(CharsetsUtils.UTF_8), type);
            Mac mac = Mac.getInstance(type);
            mac.init(secretKey);
            return mac.doFinal(str.getBytes(CharsetsUtils.UTF_8));
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>执行Mac加密失败[{}]：{}<<<<<<<<", str, e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HmacManageComponent class

/* End of file HmacManageComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/HmacManageComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
