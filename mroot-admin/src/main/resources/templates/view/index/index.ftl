<@OVERRIDE name="MAIN_CONTENT">

    <section class="section section--white section--padding">

        <div class="content">

            <div class="contents contents-sidebar-wide">

                <div class="contents__box contents__box--left">

                    <ul id="changelog" class="accordion">

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2019-07-17"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2019年07月17日 发布 2.5 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2019-07-17" class="collapse" role="tabpanel">

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> Spring Boot 到 2.1.6 版本</li>
                                    <li>升级 <code>2</code> sofaboot 到 3.1.5 版本</li>
                                    <li>升级 <code>3</code> hibernate-validator 到 6.0.17 版本</li>
                                    <li>升级 <code>4</code> mybatisplus 到 3.1.2 版本</li>
                                    <li>升级 <code>5</code> commons-collections4 到 4.4 版本</li>
                                    <li>升级 <code>6</code> guava 到 28.0 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整
                                <p>
                                <ul>
                                    <li>调整 <code>1</code> 代码生成模板</li>
                                    <li>调整 <code>2</code> mybatisplus配置、分页排序处理</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复
                                <p>
                                <ul>
                                    <li>修复 <code>1</code> 后台管理模块博客详情页面没有正确读取文章封面图片</li>
                                </ul>
                                <div class="space"></div>

                                <p>优化
                                <p>
                                <ul>
                                    <li>优化 <code>1</code> i18n 文件</li>
                                </ul>
                                <div class="space"></div>

                                <p>注
                                <p>
                                <ul>
                                    <li>注 <code>1</code> 博客预览传送门 <a href="http://blog.yuneryu.com/">http://blog.yuneryu.com/</a>
                                    </li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2019-05-22"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2019年05月22日 发布 2.4 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2019-05-22" class="collapse" role="tabpanel">

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> Spring Boot 到 2.1.5 版本</li>
                                    <li>升级 <code>2</code> commons-lang3 到 3.9 版本</li>
                                    <li>升级 <code>3</code> common-fileupload 到 1.4 版本</li>
                                    <li>升级 <code>4</code> mysql-connector-java 到 8.0.16 版本</li>
                                    <li>升级 <code>5</code> okhttp 到 3.14.2 版本</li>
                                    <li>升级 <code>6</code> maven-compiler-plugin 到 3.8.1 版本</li>
                                    <li>升级 <code>7</code> lombok 到 1.18.8 版本</li>
                                    <li>升级 <code>8</code> slf4j 到 1.7.26 版本</li>
                                    <li>升级 <code>9</code> fastjson 到 1.2.58 版本</li>
                                    <li>升级 <code>10</code> aspectjrt 到 1.9.4 版本</li>
                                    <li>升级 <code>11</code> jsoup 到 1.12.1 版本</li>
                                    <li>升级 <code>12</code> common-codec 到 1.12 版本</li>
                                    <li>升级 <code>13</code> shiro 到 1.4.1 版本</li>
                                    <li>升级 <code>14</code> shiro-freemarker-tags 到 1.0.0 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> 后台管理模块更新文章时，博客模块的文章缓存也更新</li>
                                    <li>新增 <code>2</code> 后台管理模块更新文章分类时，清空博客模块的文章分类缓存</li>
                                    <li>新增 <code>3</code> 博客模块可以读取三级文章分类的文章</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整
                                <p>
                                <ul>
                                    <li>调整 <code>1</code> 后台管理模块文章分类的标题可以使用空格</li>
                                    <li>调整 <code>2</code> 后台管理模块备注可以使用空格</li>
                                    <li>调整 <code>3</code> 后台管理模块文章的标题可以使用符号（？?，,。.、 ：:！!“”"）</li>
                                    <li>调整 <code>4</code> 后台管理模块新增文章时，可以新增所有分类下的文章</li>
                                    <li>调整 <code>5</code> 后台管理模块默认使用 tomcat 容器</li>
                                    <li>调整 <code>6</code> pom文件</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复
                                <p>
                                <ul>
                                    <li>修复 <code>1</code> 后台管理模块表单页面无法返回列表页面</li>
                                    <li>修复 <code>2</code> 博客模块读取分类文章错误</li>
                                    <li>修复 <code>3</code> 后台管理模块修改文章时，没有修改描述</li>
                                    <li>修复 <code>4</code> 后台管理模块文章表单，文章标题出现"时，不能正常显示标题</li>
                                </ul>
                                <div class="space"></div>

                                <p>优化
                                <p>
                                <ul>
                                    <li>优化 <code>1</code> 博客模块样式，更好的适配移动端</li>
                                </ul>
                                <div class="space"></div>

                                <p>注
                                <p>
                                <ul>
                                    <li>注 <code>1</code> 博客预览传送门 <a href="http://blog.yuneryu.com/">http://blog.yuneryu.com/</a>
                                    </li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2019-05-04"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2019年05月04日 发布 2.3 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2019-05-04" class="collapse" role="tabpanel">

                                <p>重大
                                <p>
                                <ul>
                                    <li>重大 <code>1</code> 新增一套博客系统</li>
                                    <li>重大 <code>2</code> 多数据源支持</li>
                                </ul>
                                <div class="space"></div>

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> FastJSON 到 1.2.57 版本</li>
                                    <li>升级 <code>2</code> SOFABoot 到 3.1.3 版本</li>
                                    <li>升级 <code>3</code> MyBatis-Plus 到 3.1.1 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> 后台数据库系统配置更新时，博客模块的系统配置也更新</li>
                                    <li>新增 <code>2</code> 后台清空某些缓存，而不是全部缓存</li>
                                    <li>新增 <code>3</code> 文章的是否转载和是否打赏功能</li>
                                    <li>新增 <code>4</code> OkHttp3Utils工具类</li>
                                    <li>新增 <code>5</code> 多数据源支持</li>
                                    <li>新增 <code>6</code> ip2region 的信息</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整
                                <p>
                                <ul>
                                    <li>调整 <code>1</code> 后台模块的资源文件结构</li>
                                    <li>调整 <code>2</code> 系统配置内容的验证规则，可以输入-、空格和中文</li>
                                    <li>调整 <code>3</code> 上传图片的问题</li>
                                    <li>调整 <code>4</code> 后台分页组件</li>
                                    <li>调整 <code>5</code> 后台的访问统计</li>
                                    <li>调整 <code>6</code> 后台 Redis 的缓存管理</li>
                                    <li>调整 <code>7</code> 模块启动的说明</li>
                                    <li>调整 <code>8</code> 公共缓存 key 的名称</li>
                                    <li>调整 <code>9</code> 缓存的管理</li>
                                    <li>调整 <code>10</code> 后台文章描述的显示</li>
                                    <li>调整 <code>11</code> 不同的 web 模块使用不同的容器</li>
                                    <li>调整 <code>12</code> 博客使用 Hikari 连接池</li>
                                    <li>调整 <code>13</code> hibernate-validator 的引入</li>
                                    <li>调整 <code>14</code> 项目启动后的任务</li>
                                    <li>调整 <code>15</code> pom文件</li>
                                    <li>调整 <code>16</code> 验证码</li>
                                    <li>调整 <code>17</code> jar 文件的读取</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复
                                <p>
                                <ul>
                                    <li>修复 <code>1</code> 后台模块系统启动后报错</li>
                                    <li>修复 <code>2</code> 数据库配置服务的是否开启错误</li>
                                </ul>
                                <div class="space"></div>

                                <p>注
                                <p>
                                <ul>
                                    <li>注 <code>1</code> 后台 DEFAULT 风格不在进行维护
                                    </li>
                                    <li>注 <code>2</code> 博客预览传送门 <a href="http://blog.yuneryu.com/">http://blog.yuneryu.com/</a>
                                    </li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2019-04-10"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2019年04月10日 发布 2.2 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2019-04-10" class="collapse" role="tabpanel">

                                <p>重大
                                <p>
                                <ul>
                                    <li>重大 <code>1</code> Ehcache3 改为 Redis 以便更好的集群部署</li>
                                    <li>重大 <code>2</code> Elite Admin 风格的 UI 界面</li>
                                </ul>
                                <div class="space"></div>

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> Druid 到 1.1.16 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> Redis</li>
                                    <li>新增 <code>2</code> Elite Admin 风格的 UI 界面</li>
                                    <li>新增 <code>3</code> 邮箱记录管理</li>
                                    <li>新增 <code>4</code> 异步任务，添加邮箱记录</li>
                                    <li>新增 <code>5</code> 使用 Redis 使 Session 同步</li>
                                    <li>新增 <code>6</code> 文章分类列表可以直接添加文章</li>
                                </ul>
                                <div class="space"></div>

                                <p>优化
                                <p>
                                <ul>
                                    <li>优化 <code>1</code> 首页</li>
                                    <li>优化 <code>2</code> 多种 UI 共存</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整
                                <p>
                                <ul>
                                    <li>调整 <code>1</code> 首页的描述</li>
                                    <li>调整 <code>2</code> PathUtils 中获取 classpath 的方法</li>
                                    <li>调整 <code>3</code> 项目打包成 jar 包</li>
                                    <li>调整 <code>4</code> 系统配置的标识不能进行修改</li>
                                    <li>调整 <code>5</code> Dozer 的配置</li>
                                    <li>调整 <code>6</code> 系统启动的日志级别</li>
                                    <li>调整 <code>7</code> 国际化信息</li>
                                    <li>调整 <code>8</code> 代码模板</li>
                                    <li>调整 <code>9</code> 取消 Shiro url 后面的 JSESSIONID</li>
                                    <li>调整 <code>10</code> 登录成功后返回的数据类型</li>
                                    <li>调整 <code>11</code> 封面的上传后端处理</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复
                                <p>
                                <ul>
                                    <li>修复 <code>1</code> 日志列表表格样式错误</li>
                                    <li>修复 <code>2</code> 上传文章封面的地址错误</li>
                                    <li>修复 <code>3</code> 更新文章时，没有更新封面</li>
                                </ul>
                                <div class="space"></div>

                                <p>注
                                <p>
                                <ul>
                                    <li>注 <code>1</code> 修改风格 mroot-admin 模块下的 resource.properties 中的<br>
                                        resource.current-theme=ELITE 值取 ELITE、DEFAULT
                                    </li>
                                    <li>注 <code>2</code> 须安装 Redis 数据库</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2019-03-26"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2019年03月26日 发布 2.1 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2019-03-26" class="collapse" role="tabpanel">

                                <p>重大
                                <p>
                                <ul>
                                    <li>重大 <code>1</code> Java11 改为 Java8</li>
                                </ul>
                                <div class="space"></div>

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> SOFABoot 到 3.1.2 版本</li>
                                    <li>升级 <code>2</code> MyBatis-Plus 到 3.1.0 版本</li>
                                    <li>升级 <code>3</code> Spring Boot 到 2.1.3 版本</li>
                                    <li>升级 <code>4</code> FastJSON 到 1.2.56 版本</li>
                                    <li>升级 <code>5</code> Druid 到 1.1.14 版本</li>
                                    <li>升级 <code>6</code> Hibernate Validator 到 6.0.16 版本</li>
                                    <li>升级 <code>7</code> Guava 到 27.1 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> list 方法</li>
                                    <li>新增 <code>2</code> 邮箱功能</li>
                                    <li>新增 <code>3</code> 异步任务的处理结果</li>
                                    <li>新增 <code>4</code> 一些工具类</li>
                                </ul>
                                <div class="space"></div>

                                <p>优化
                                <p>
                                <ul>
                                    <li>优化 <code>1</code> 优化架构</li>
                                    <li>优化 <code>2</code> Shiro 权限匹配</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整
                                <p>
                                <ul>
                                    <li>调整 <code>1</code> 代码模板</li>
                                    <li>调整 <code>2</code> ResultData 返回数据</li>
                                    <li>调整 <code>3</code> 代码生成</li>
                                    <li>调整 <code>4</code> Shiro 不拦截首页</li>
                                    <li>调整 <code>5</code> 文章</li>
                                    <li>调整 <code>6</code> 首页</li>
                                    <li>调整 <code>7</code> Druid 采用 druid-spring-boot-starter 引入</li>
                                    <li>调整 <code>8</code> 缓存的处理</li>
                                    <li>调整 <code>9</code> 拦截器的处理</li>
                                    <li>调整 <code>10</code> 国际化的处理</li>
                                    <li>调整 <code>11</code> 线程池和定时任务的处理</li>
                                    <li>调整 <code>12</code> 角色处理</li>
                                    <li>调整 <code>13</code> 配置处理</li>
                                    <li>调整 <code>14</code> 定时任务处理</li>
                                    <li>调整 <code>15</code> 上传处理</li>
                                </ul>
                                <div class="space"></div>

                                <p>移除
                                <p>
                                <ul>
                                    <li>移除 <code>1</code> 无用的资源文件</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2018-12-17"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2018年12月17日 发布 2.0.1 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2018-12-17" class="collapse" role="tabpanel">

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> 默认首页的设置</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整</p>
                                <ul>
                                    <li>调整 <code>1</code> 权限( system_rule )表的 url 的字段加上唯一限制</li>
                                    <li>调整 <code>2</code> 添加子级权限时，去掉标识字段</li>
                                    <li>调整 <code>3</code> 代码生成模板</li>
                                    <li>调整 <code>4</code> 插入或更新的字段有空字符串或者 null 时 FieldStrategy 的策略，默认是 NOT_EMPTY</li>
                                    <li>调整 <code>5</code> 开发阶段不把缓存实例化到硬盘上</li>
                                    <li>调整 <code>6</code> JQuery 插件的中文提示</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复
                                <p>
                                <ul>
                                    <li>修复 <code>1</code> 登录后首页条数没有数据</li>
                                    <li>修复 <code>2</code> Shiro 权限匹配</li>
                                    <li>修复 <code>3</code> 使用 Spring Boot 内嵌的服务容器启动项目时，无法初始化</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2018-12-09"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2018年12月09日 发布 2.0 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2018-12-09" class="collapse" role="tabpanel">

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> Kotlin 改为 Java 11</li>
                                    <li>升级 <code>2</code> MyBatisPlus-Boot 到 3.0-RELEASE 版本</li>
                                    <li>升级 <code>3</code> Spring Boot 到 2.1.1 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整</p>
                                <ul>
                                    <li>调整 <code>1</code> 由 Kotlin 改为 Java 实现</li>
                                    <li>调整 <code>2</code> 按照 阿里巴巴Java代码规范 进行开发</li>
                                    <li>调整 <code>3</code> 调整实体类，分为 DO、BO 和 VO</li>
                                    <li>调整 <code>4</code> 架构分层更为清晰</li>
                                    <li>调整 <code>5</code> 引入 SOFABoot</li>
                                    <li>调整 <code>6</code> 数据库更好的支持 MariaDB</li>
                                </ul>
                                <div class="space"></div>

                                <p>说明</p>
                                <ul>
                                    <li>说明 <code>1</code> 用 Java11 开发的，实际上没有用到 Java11 的新特性，Java8 一样可以编译</li>
                                    <li>说明 <code>2</code> 引入 SOFABoot 是为了以后更好的融入阿里的一些组件</li>
                                </ul>
                                <div class="space"></div>

                                <p>感谢</p>
                                <ul>
                                    <li>感谢 <code>1</code> VJTools</li>
                                    <li>感谢 <code>2</code> https://my.oschina.net/dreamlove/blog/490250</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2018-08-29"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2018年08月29日 发布 1.2 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2018-08-29" class="collapse" role="tabpanel">

                                <p>升级</p>
                                <ul>
                                    <li>升级 <code>1</code> Kotlin 到 1.2.60 版本</li>
                                    <li>升级 <code>2</code> MyBatisPlus-Boot 到 3.0-RELEASE 版本</li>
                                    <li>升级 <code>3</code> Spring Boot 到 2.0.4 版本</li>
                                    <li>升级 <code>4</code> Hibernate-Validator到 6.0.12.Final 版本</li>
                                    <li>升级 <code>5</code> FastJSON 到 1.2.49 版本</li>
                                    <li>升级 <code>6</code> commons-lang3 到 3.8 版本</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整</p>
                                <ul>
                                    <li>调整 <code>1</code> model 模块改名为 do 模块</li>
                                    <li>调整 <code>2</code> mapper 模块改为 dao 模块</li>
                                    <li>调整 <code>3</code> 新增 BO、VO 层(其它层的数据处理都发生变化)</li>
                                    <li>调整 <code>4</code> 事件驱动使用自定义线程池</li>
                                    <li>调整 <code>5</code> 百度编辑器</li>
                                    <li>调整 <code>6</code> 优化 Request 的使用</li>
                                    <li>调整 <code>7</code> Profile类 改为静态访问</li>
                                    <li>调整 <code>8</code> AesManage 类改为静态访问</li>
                                    <li>调整 <code>9</code> DigestManage 类改为静态访问</li>
                                    <li>调整 <code>10</code> LocaleMessageSource 类改为静态访问</li>
                                    <li>调整 <code>11</code> QiNiu类 改为静态访问</li>
                                    <li>调整 <code>12</code> 采用全新的 Logo</li>
                                    <li>调整 <code>13</code> 其它细节优化</li>
                                </ul>
                                <div class="space"></div>

                                <p>数据库</p>
                                <ul>
                                    <li>数据库 <code>1</code> 数据库由 MySql 8 变成 MariaDB 10.3</li>
                                    <li>数据库 <code>2</code> 数据库编码改为 utf8mb4_general_ci</li>
                                    <li>数据库 <code>3</code> 表字段的改变</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2018-06-29"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2018年06月29日 发布 1.1.0 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2018-06-29" class="collapse" role="tabpanel">

                                <p>新增</p>
                                <ul>
                                    <li>新增 <code>1</code> 资源文件，通过配置使用七牛云</li>
                                    <li>新增 <code>2</code> 上传文件，通过配置使用七牛云</li>
                                </ul>
                                <div class="space"></div>

                                <p>调整</p>
                                <ul>
                                    <li>调整 <code>1</code> 调整首页不自动跳转到登录页面</li>
                                    <li>调整 <code>2</code> 系统启动后，异步初始化定时任务</li>
                                    <li>调整 <code>3</code> 其它细节优化</li>
                                </ul>
                                <div class="space"></div>

                                <p>修复</p>
                                <ul>
                                    <li>修复 <code>1</code> 修复上传图片的地址无法访问</li>
                                    <li>修复 <code>2</code> 修复正式环境下，代码生成管理 点击 生成没有给出正确的提示信息</li>
                                </ul>
                                <div class="space"></div>

                            </div>
                        </li>

                        <li class="card accordion__item">
                            <a class="collapsed" data-toggle="collapse" data-parent="#changelog" href="#v2018-06-20"
                               aria-expanded="false">
                                <div class="accordion__item_top">
                                    <h5>2018年06月20日 发布 1.0.0 版本</h5>
                                    <i class="accordion__item_top_icon"></i>
                                </div>
                            </a>
                            <div id="v2018-06-20" class="collapse" role="tabpanel">
                                <p>所有模块</p>
                                <ul>
                                    <li><code>1</code>用户管理模块</li>
                                    <li><code>2</code>角色管理模块</li>
                                    <li><code>3</code>权限管理模块</li>
                                    <li><code>4</code>代码生成模块</li>
                                    <li><code>5</code>系统配置模块</li>
                                    <li><code>6</code>定时任务模块</li>
                                    <li><code>7</code>系统记录模块</li>
                                    <li><code>8</code>文章模块</li>
                                </ul>
                                <div class="space"></div>
                            </div>
                        </li>

                    </ul>


                </div>


            </div>

        </div>

    </section>

</@OVERRIDE>

<@EXTENDS name="/index/base.ftl"/>
