/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import wang.encoding.mroot.common.util.collection.ListUtils;

import java.util.*;

/**
 * 使 AuthorizingRealm 支持复杂表达式（使用逆波兰表达式计算）
 *
 * @author ErYang
 */
@Slf4j
public abstract class AbstractAuthorizingRealmWithRpn extends AuthorizingRealm {


    /**
     * 支持的运算符和运算符优先级
     */
    private static final Map<String, Integer> EXP_MAP = new HashMap<String, Integer>() {
        private static final long serialVersionUID = -8168609720874009592L;

        {
            put("not", 0);
            put("!", 0);

            put("and", 0);
            put("&&", 0);

            put("or", 0);
            put("||", 0);

            put("(", 1);
            put(")", 1);
        }
    };

    // -------------------------------------------------------------------------------------------------

    private static final Set<String> EXP_LIST = EXP_MAP.keySet();

    // -------------------------------------------------------------------------------------------------

    @Override
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        Stack<String> exp = AbstractAuthorizingRealmWithRpn.getExp(permission);
        if (1 == exp.size()) {
            return super.isPermitted(principals, exp.pop());
        }
        List<String> expTemp = ListUtils.newArrayList();
        // 将其中的权限字符串解析成true/false
        for (String temp : exp) {
            if (EXP_LIST.contains(temp)) {
                expTemp.add(temp);
            } else {
                expTemp.add(Boolean.toString(super.isPermitted(principals, temp)));
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>Permission[{}],Rpn:[{}],Parse:[{}]<<<<<<<<", permission, exp, expTemp);
        }
        // 计算逆波兰
        return AbstractAuthorizingRealmWithRpn.computeRpn(expTemp);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 计算逆波兰
     * @param exp Collection<String>
     * @return boolean
     */
    private static boolean computeRpn(final Collection<String> exp) {
        Stack<Boolean> stack = new Stack<>();
        for (String temp : exp) {
            if (((Collection<String>) AbstractAuthorizingRealmWithRpn.EXP_LIST).contains(temp)) {
                if ("!" .equals(temp) || "not" .equals(temp)) {
                    stack.push(!stack.pop());
                } else if ("and" .equals(temp) || "&&" .equals(temp)) {
                    Boolean s1 = stack.pop();
                    Boolean s2 = stack.pop();
                    stack.push(s1 && s2);
                } else {
                    Boolean s1 = stack.pop();
                    Boolean s2 = stack.pop();
                    stack.push(s1 || s2);
                }
            } else {
                stack.push(Boolean.parseBoolean(temp));
            }
        }
        if (1 < stack.size()) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>ComputeRpn>{},Exp[{}]<<<<<<<<", stack, exp);
            }
            throw new RuntimeException(exp.toString());
        } else {
            return stack.pop();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得逆波兰表达式
     *
     * @param exp String
     * @return Stack<String>
     */
    private static Stack<String> getExp(final String exp) {
        Stack<String> s1 = new Stack<>();
        Stack<String> s2 = new Stack<>();
        String nullV = " ";
        for (String str : exp.split(nullV)) {
            str = str.trim();
            String strL = str.toLowerCase();
            if ("" .equals(str)) {
                continue;
            }
            if ("(" .equals(str)) {
                //左括号
                s1.push(str);
            } else if (")" .equals(str)) {
                //右括号
                while (!s1.empty()) {
                    String temp = s1.pop();
                    if ("(" .equals(temp)) {
                        break;
                    } else {
                        s2.push(temp);
                    }
                }
            } else if (((Collection<String>) AbstractAuthorizingRealmWithRpn.EXP_LIST).contains(strL)) {
                //操作符
                if (s1.empty()) {
                    s1.push(strL);
                } else {
                    String temp = s1.peek();
                    if ("(" .equals(temp) || ")" .equals(temp)) {
                        s1.push(strL);
                    } else if (EXP_MAP.get(strL) >= EXP_MAP.get(temp)) {
                        s1.push(strL);
                    } else {
                        s2.push(s1.pop());
                        s1.push(strL);
                    }
                }
            } else {
                //运算数
                s2.push(str);
            }
        }
        while (!s1.empty()) {
            s2.push(s1.pop());
        }
        return s2;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AbstractAuthorizingRealmWithRpn class

/* End of file AbstractAuthorizingRealmWithRpn.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/AbstractAuthorizingRealmWithRpn.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
