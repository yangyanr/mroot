/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms.impl;


import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.dao.cms.CategoryDAO;
import wang.encoding.mroot.domain.entity.cms.CategoryDO;
import wang.encoding.mroot.service.cms.CategoryService;

import java.math.BigInteger;
import java.util.List;

/**
 * 后台 文章分类 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class CategoryServiceImpl extends BaseServiceImpl<CategoryDAO, CategoryDO> implements CategoryService {


    /**
     * 得到 类型小于 3 文章分类集合
     *
     * @return List
     */
    @Override
    public List<CategoryDO> listTypeLt3() {
        return baseMapper.listTypeLt3();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 文章分类集合
     *
     * @return List
     */
    @Override
    public List<CategoryDO> listType() {
        return baseMapper.listType();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据父类 id 查询子类 id
     *
     * @param  id BigInteger
     *
     * @return List
     */
    @Override
    public List<BigInteger> listByPId(@NotNull final BigInteger id) {
        return baseMapper.listByPId(id);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryServiceImpl class

/* End of file CategoryServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/cms/impl/CategoryServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
