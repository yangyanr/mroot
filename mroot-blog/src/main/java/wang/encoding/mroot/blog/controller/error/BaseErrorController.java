/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.controller.error;


import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.controller.BlogBaseController;

/**
 * 错误控制器
 *
 * @author ErYang
 */
@RestController
public class BaseErrorController extends BlogBaseController implements ErrorController {


    /**
     * 错误页面地址
     */
    private static final String ERROR_URL = "/error";
    /**
     * 404错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";
    /**
     * 错误视图
     */
    private static final String ERROR_EXCEPTION_VIEW = "/error/404";
    /**
     * Oops页面
     */
    private static final String OOPS = "/error/oops";
    private static final String OOPS_URL = OOPS;
    private static final String OOPS_URL_VIEW = OOPS;


    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR404_URL)
    public ModelAndView error404() {
        return new ModelAndView(super.initView(ERROR_EXCEPTION_VIEW));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR_URL)
    public ModelAndView handleError404() {
        return new ModelAndView(super.initView(ERROR_EXCEPTION_VIEW));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Oops 页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = OOPS_URL)
    public ModelAndView oops() {
        return new ModelAndView(super.initView(OOPS_URL_VIEW));
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public String getErrorPath() {
        return super.getCurrentThemePath() + ERROR_EXCEPTION_VIEW;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseErrorController class

/* End of file BaseErrorController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/controller/error/BaseErrorController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
