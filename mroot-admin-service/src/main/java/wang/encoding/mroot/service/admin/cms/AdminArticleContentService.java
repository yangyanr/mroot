/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.cms;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.cms.articlecontent.AdminArticleContentGetVO;

import java.math.BigInteger;
import java.util.List;

/**
 * 后台 文章内容 Service 接口
 *
 * @author ErYang
 */
public interface AdminArticleContentService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminArticleContentGetVO
     */
    AdminArticleContentGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章内容
     *
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章内容
     *
     * @param articleContentGetVO AdminArticleContentGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminArticleContentGetVO articleContentGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章内容 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminArticleContentGetVO>
     * @param adminArticleContentGetVO AdminArticleContentGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminArticleContentGetVO>
     */
    IPage<AdminArticleContentGetVO> list2page(@NotNull final Page<AdminArticleContentGetVO> pageAdmin,
            @NotNull final AdminArticleContentGetVO adminArticleContentGetVO, @Nullable String orderByField,
            boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     *
     * @return String
     */
    String validationArticleContent(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminArticleContentGetVO 列表
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     * @return List
     */
    List<AdminArticleContentGetVO> list(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminArticleContentService interface

/* End of file AdminArticleContentService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/cms/AdminArticleContent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
