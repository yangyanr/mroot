<@OVERRIDE name="MAIN_CONTENT">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.form.head.title")}
    </h3>
    </div>
    </div>
    </div>

    <form class="m-form m-form--state m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.id")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static">${article.id}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.categoryId")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.title></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.title")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.title></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.description")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.description></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.articleContent.form.content")}
    </label>
    <div class="col-lg-9 col-md-9 col-sm-12">
    <span class="m-form__control-static">
    <@defaultStr articleContent.content></@defaultStr>
    </span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.cover")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static">
    <#if article.cover??>
        <img style="width: 120px;height: 120px;" src="${article.cover}">
    <#else>
        <@defaultStr article.cover></@defaultStr>
    </#if>
    </span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.pageView")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.pageView></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.priority")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.priority></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.linkUri")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.linkUri></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.display")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@boole article.display></@boole></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.state")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr  article.status></@defaultStr></span>
    </div>
    </div>

<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtCreate")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@dateFormat  article.gmtCreate></@dateFormat></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtCreateIp")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr article.ip></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtModified")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@dateFormat  article.gmtModified></@dateFormat></span>
    </div>
    </div>

    </div>

    <@viewFormOperate></@viewFormOperate>

    </form>

    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
