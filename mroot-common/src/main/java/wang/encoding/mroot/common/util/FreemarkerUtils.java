/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.io.PathUtils;

import java.io.*;
import java.util.Locale;
import java.util.Map;

/**
 * Freemarker 工具类
 *
 * @author ErYang
 */
@Slf4j
public class FreemarkerUtils {

    /**
     * 打印到控制台(测试用)
     *
     * @param ftlName String
     * @param root Map
     * @param ftlPath String
     */
    public static void print(@NotNull final String ftlName, @NotNull final Map<String, Object> root,
            @NotNull final String ftlPath) {
        try {
            // 通过Template可以将模板文件输出到相应的流
            Template temp = FreemarkerUtils.getTemplate(ftlName, ftlPath);
            temp.process(root, new PrintWriter(System.out));
        } catch (TemplateException | IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>打印到控制台[{},{}]<<<<<<<<", ftlName, ftlPath);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 输出到文件
     *
     * @param ftlName  String ftl文件名
     * @param root     Map 传入的 map
     * @param outFile  String 输出后的文件全部路径
     * @param filePath String 输出前的文件上部路径
     */
    public static void printFile(@NotNull final String ftlName, @NotNull final Map<String, Object> root,
            @NotNull final String outFile, @NotNull final String filePath, @NotNull final String ftlPath) {
        try {

            File file = new File(PathUtils.getClassPath() + filePath + outFile);
            // 判断有没有父路径，就是判断文件整个路径是否存在
            if (!file.getParentFile().exists()) {
                // 不存在就全部创建
                boolean flag = file.getParentFile().mkdirs();
                if (flag) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(">>>>>>>>创建[{}]目录成功<<<<<<<<", outFile);
                    }
                } else {
                    if (logger.isErrorEnabled()) {
                        logger.error(">>>>>>>>创建[{}]目录失败<<<<<<<<", outFile);
                    }
                }
            }
            Writer out = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(file), CharsetsUtils.UTF_8_NAME));
            Template template = getTemplate(ftlName, ftlPath);
            //模版输出
            template.process(root, out);
            out.flush();
            out.close();

        } catch (TemplateException | IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>输出到文件失败[{}]<<<<<<<<", ftlName);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 通过文件名加载模版
     *
     * @param name    String 文件名
     * @param ftlPath String 路径
     * @return Template 模板
     */
    private static Template getTemplate(@NotNull final String name, @NotNull final String ftlPath) {
        try {
            // 通过 freemarker 的Configuration读取相应的ftl
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
            cfg.setEncoding(Locale.CHINA, CharsetsUtils.UTF_8_NAME);
            // 设定去哪里读取相应的模板文件
            cfg.setDirectoryForTemplateLoading(new File(PathUtils.getClassPath() + ftlPath));
            // 在模板文件目录中找到名称为name的文件
            return cfg.getTemplate(name);
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>通过文件名加载模版失败[{},{}]<<<<<<<<", name, ftlPath, e);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -----------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FreemarkerUtils class

/* End of file FreemarkerUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/FreemarkerUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
