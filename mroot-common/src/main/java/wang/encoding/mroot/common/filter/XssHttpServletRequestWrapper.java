/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.filter;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.util.Jsoup2XssUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 重写 HttpServletRequestWrapper XssHttpServletRequestWrapper
 *
 * @author ErYang
 */
@Slf4j
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private static final String CONTENT_NAME = "content";
    private static final String WITH_HTML_NAME = "WithHtml";
    /**
     * 获取最原始的 request
     *
     * @return HttpServletRequest
     */
    private HttpServletRequest originalRequest;

    private boolean isIncludeRichText;


    // -------------------------------------------------------------------------------------------------

    XssHttpServletRequestWrapper(HttpServletRequest request, boolean isIncludeRichText) {
        super(request);
        originalRequest = request;
        this.isIncludeRichText = isIncludeRichText;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 覆盖getHeader方法，将参数名和参数值都做xss过滤
     * 如果需要获得原始的值，则通过super.getHeaders(name)来获取
     * getHeaderNames 也可能需要覆盖
     *
     * @param name String
     * @return String
     */
    @Override
    public String getHeader(String name) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>getHeader进行过滤[{}]>>>>>>>>", name);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤前的名称[{}]>>>>>>>>", name);
        }
        name = Jsoup2XssUtils.clean(name);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤后的名称[{}]>>>>>>>>", name);
        }
        String value = super.getHeader(name);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤前的值[{}]>>>>>>>>", value);
        }
        if (StringUtils.isNotBlank(value)) {
            value = Jsoup2XssUtils.clean(value);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤后的值[{}]>>>>>>>>", value);
        }
        return value;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 覆盖getParameter方法，将参数名和参数值都做xss过滤
     * 如果需要获得原始的值，则通过super.getParameterValues(name)来获取
     * getParameterNames,getParameterValues和getParameterMap也可能需要覆盖
     *
     * @param name String
     * @return String
     */
    @Override
    public String getParameter(String name) {
        boolean flag = (CONTENT_NAME.equals(name) || name.endsWith(WITH_HTML_NAME));
        if (flag && !isIncludeRichText) {
            return super.getParameter(name);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>getParameter进行过滤[{}]>>>>>>>>", name);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤前的名称[{}]>>>>>>>>", name);
        }
        name = Jsoup2XssUtils.clean(name);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤后的名称[{}]>>>>>>>>", name);
        }
        String value = super.getParameter(name);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤前的值[{}]>>>>>>>>", value);
        }
        if (StringUtils.isNotBlank(value)) {
            value = Jsoup2XssUtils.clean(value);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>过滤后的值[{}]>>>>>>>>", value);
        }
        return value;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 覆盖getParameterValues方法
     * 如果需要获得原始的值，则通过super.getParameterValues(name)来获取
     * getParameterNames,getParameterValues和getParameterMap也可能需要覆盖
     *
     * @param name String
     * @return String[]
     */
    @Override
    public String[] getParameterValues(String name) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>getParameterValues进行过滤[{}]>>>>>>>>", name);
        }
        String[] arr = super.getParameterValues(name);
        if (ArrayUtils.isNotEmpty(arr)) {
            int i;
            int length = arr.length;
            for (i = 0; i < length; i++) {
                arr[i] = Jsoup2XssUtils.clean(arr[i]);
            }
        }
        return arr;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取最原始的 request 的静态方法
     *
     * @param request HttpServletRequest
     * @return HttpServletRequest
     */
    public static HttpServletRequest originalRequest(final HttpServletRequest request) {
        if (request instanceof XssHttpServletRequestWrapper) {
            return ((XssHttpServletRequestWrapper) request).getOriginalRequest();
        } else {
            return request;
        }
    }

    // -------------------------------------------------------------------------------------------------

    public HttpServletRequest getOriginalRequest() {
        return originalRequest;
    }

    public void setOriginalRequest(HttpServletRequest originalRequest) {
        this.originalRequest = originalRequest;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End XssHttpServletRequestWrapper class

/* End of file XssHttpServletRequestWrapper.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/xss/XssHttpServletRequestWrapper.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
