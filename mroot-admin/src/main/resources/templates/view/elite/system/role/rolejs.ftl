<script>

    /**
     * 角色提示信息
     */
    var RoleValidation = function () {

        var name_pattern = '${I18N("jquery.validation.sole.pattern")}';
        var title_pattern = '${I18N("jquery.validation.title.pattern")}';
        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';

        return {

            getNamePattern: function () {
                return name_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getRemarkPattern: function () {
                return remark_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
