/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.autoconfigure.http.converter;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import lombok.extern.slf4j.Slf4j;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.reflect.ClassUtils;

import java.util.*;


/**
 * 配置 fastjson 作为数据返回转换
 *
 * 如果没有配置 spring.http.converters.preferred-json-mapper 参数则使用该配置进行转换数据返回
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
@ConditionalOnClass({FastJsonHttpMessageConverter.class, ConfigurationBuilder.class})
@AutoConfigureBefore(HttpMessageConvertersAutoConfiguration.class)
@ConditionalOnProperty(prefix = "spring.http.converters", value = {
        "preferred-json-mapper"}, havingValue = "fastJson", matchIfMissing = true)
@Slf4j
public class HttpMessageConverterAutoConfiguration {

    /**
     * 值过滤器包
     */
    private static final String[] VALUE_FILTER_PACKAGE = new String[]{
            "wang.encoding.mroot.plugin.http.converter.filter"};

    // -------------------------------------------------------------------------------------------------

    /**
     * 注入 bean 工厂
     */
    private final BeanFactory beanFactory;

    @Autowired
    public HttpMessageConverterAutoConfiguration(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * http message converter fastjson 实现实例
     * 通过 fastjson 方式进行格式化返回 json 字符串
     *
     * @return HttpMessageConverters
     */
    @Bean
    @ConditionalOnMissingBean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>FastJsonConfiguration设置<<<<<<<<");
        }
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //创建 fastJson 配置实体类
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.DisableCircularReferenceDetect,
                SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero,
                SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteNullBooleanAsFalse);

        // 自定义注解
        List<String> packages = AutoConfigurationPackages.get(beanFactory);
        // 过滤器
        packages.addAll(Arrays.asList(VALUE_FILTER_PACKAGE));

        fastJsonConfig.setSerializeFilters(getDefineFilters(packages));

        fastConverter.setFastJsonConfig(fastJsonConfig);
        return new HttpMessageConverters(fastConverter);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取项目中定义的 ValueFilter 实现类列表
     * 通过 BeanFactory 读取本项目的 Base Package List
     *
     * @param packages List
     * @return ValueFilter 数组 ValueFilter[]
     */
    private ValueFilter[] getDefineFilters(@NotNull final List<String> packages) {
        Set<Class> filterClass = new HashSet<>();
        if (ObjectUtils.isEmpty(packages)) {
            return new ValueFilter[]{};
        }
        // 读取所有 package 下的 ValueFilter 实现类
        packages.forEach(pack -> filterClass.addAll(ClassUtils.getSubClassList(pack, ValueFilter.class)));
        List<ValueFilter> filters = new LinkedList<>();
        filterClass.forEach(filter -> {
            try {
                filters.add((ValueFilter) filter.newInstance());
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>ValueFilter的接口错误<<<<<<<<", e);
                }
            }
        });
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>加载ValueFilter[{}]<<<<<<<<", filterClass.toString());
        }
        return filters.toArray(new ValueFilter[]{});
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HttpMessageConverterAutoConfiguration class

/* End of file HttpMessageConverterAutoConfiguration.java */
/* Location: ./src/main/java/wang/encoding/mroot/autoconfigure/http/converter/HttpMessageConverterAutoConfiguration.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
