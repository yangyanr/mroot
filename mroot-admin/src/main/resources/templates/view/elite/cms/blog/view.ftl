<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/contact-app-page.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/inbox.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>


    <div class="col-12">

        <div class="row">

            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <#if article.cover??>
                        <img class="card-img" src="${article.cover}" height="456"
                             alt="${article.title}">
                    <#else>
                        <img class="card-img" src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-5.png"
                             height="456"
                             alt="${article.title}">
                    </#if>

                </div>
                <div class="card">
                    <div class="card-body">
                        <small class="text-muted">${I18N("message.cms.blog.view.category")}</small>
                        <h6><@defaultStr article.category.title></@defaultStr></h6>
                        <small class="text-muted p-t-30 db">${I18N("message.cms.blog.view.pageView")}</small>
                        <h6><@defaultStr article.pageView></@defaultStr></h6>
                        <small class="text-muted p-t-30 db">${I18N("message.cms.blog.view.datetime")}</small>
                        <h6><@dateFormat  article.gmtCreate></@dateFormat></h6>
                        <br>
                        <div class="map-box"><@defaultStr article.description></@defaultStr></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title m-b-40"><@defaultStr article.title></@defaultStr></h3>
                        <div class="d-flex m-b-40"><@defaultStr articleContent.content></@defaultStr></div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
