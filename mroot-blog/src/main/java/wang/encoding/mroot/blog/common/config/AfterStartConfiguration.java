/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask;

/**
 * 系统启动后执行
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Order(0)
public class AfterStartConfiguration implements CommandLineRunner {

    private final BlogControllerAsyncTask blogControllerAsyncTask;

    @Autowired
    public AfterStartConfiguration(BlogControllerAsyncTask blogControllerAsyncTask) {
        this.blogControllerAsyncTask = blogControllerAsyncTask;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void run(String... args) {
        System.out.println();
        System.out.println(
                "     _______.___________.    ___      .______         ____    __    ____  ___      .______          _______.\n"
                        + "    /       |           |   /   \\     |   _  \\        \\   \\  /  \\  /   / /   \\     |   _  \\        /       |\n"
                        + "   |   (----`---|  |----`  /  ^  \\    |  |_)  |        \\   \\/    \\/   / /  ^  \\    |  |_)  |      |   (----`\n"
                        + "    \\   \\       |  |      /  /_\\  \\   |      /          \\            / /  /_\\  \\   |      /        \\   \\\n"
                        + ".----)   |      |  |     /  _____  \\  |  |\\  \\----.      \\    /\\    / /  _____  \\  |  |\\  \\----.----)   |\n"
                        + "|_______/       |__|    /__/     \\__\\ | _| `._____|       \\__/  \\__/ /__/     \\__\\ | _| `._____|_______/");
        System.out.println();
        System.out.println(
                "////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("//                               Blog 启动成功");
        System.out.println(
                "////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println();
        // 初始化项目配置
        blogControllerAsyncTask.init();
        // 启动成功时 清空缓存
        blogControllerAsyncTask.clearCache();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AfterStartConfiguration class

/* End of file AfterStartConfiguration.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/config/AfterStartConfiguration.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
