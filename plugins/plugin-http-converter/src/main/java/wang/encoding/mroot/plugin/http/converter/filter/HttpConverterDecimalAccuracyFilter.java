/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.plugin.http.converter.filter;

import com.alibaba.fastjson.serializer.ValueFilter;
import org.springframework.util.ReflectionUtils;
import wang.encoding.mroot.plugin.http.converter.filter.annotation.HttpConverterDecimalAccuracy;

import java.lang.reflect.Field;
import java.math.BigDecimal;


/**
 * 小数精度过滤器
 *
 * @author ErYang
 */
public class HttpConverterDecimalAccuracyFilter implements ValueFilter {

    /**
     * 处理
     * @param object Object
     * @param name name
     * @param value value
     * @return Object
     */
    @Override
    public Object process(Object object, String name, Object value) {
        try {
            // 找到属性
            Field field = ReflectionUtils.findField(object.getClass(), name);
            // 找到 HttpConverterDecimalAccuracy Annotation
            if (field.isAnnotationPresent(HttpConverterDecimalAccuracy.class) && value instanceof BigDecimal) {
                HttpConverterDecimalAccuracy decimalAccuracy = field
                        .getDeclaredAnnotation(HttpConverterDecimalAccuracy.class);
                BigDecimal decimalValue = (BigDecimal) value;
                return decimalValue.setScale(decimalAccuracy.scale(), decimalAccuracy.roundingMode());
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HttpConverterDecimalAccuracyFilter class

/* End of file HttpConverterDecimalAccuracyFilter.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/http/converter/filter/HttpConverterDecimalAccuracyFilter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
