/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.login;


import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.DigestManageComponent;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.enums.BusinessEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.PairUtils;
import wang.encoding.mroot.common.util.text.TextValidatorUtils;
import wang.encoding.mroot.service.admin.common.constant.AdminGlobalMessage;
import wang.encoding.mroot.service.admin.system.AdminUserService;

/**
 * 登录控制器
 *
 * @author ErYang
 */
@RestController
public class LoginController extends AdminBaseController {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    /**
     * 登录 url
     */
    private static final String LOGIN_URL = "/login";
    /**
     * 处理登录 url
     */
    private static final String DO_LOGIN_URL = "/do/login";
    /**
     * 登录页面
     */
    private static final String LOGIN_VIEW = "/login/login";
    /**
     * 登录页面 开发环境
     */
    private static final String DEV_LOGIN_VIEW = "/login/login2dev";
    /**
     * 退出 url
     */
    private static final String LOGOUT_URL = "/logout";

    private final AdminUserService adminUserService;

    @Autowired
    public LoginController(AdminUserService adminUserService) {
        this.adminUserService = adminUserService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 登录页面
     *
     * @return ModelAndView
     */
    @RequestMapping(LOGIN_URL)
    @FormToken(init = true)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = new ModelAndView();
        if (this.validationVerifyCode()) {
            modelAndView.addObject("isVerifyCode", "show");
        } else {
            modelAndView.addObject("isVerifyCode", "hide");
        }
        // 开发环境
        if (ProfileComponent.isDevProfile()) {
            modelAndView.addObject("isVerifyCode", "hide");
            modelAndView.setViewName(super.initView(DEV_LOGIN_VIEW));
        } else {
            modelAndView.setViewName(super.initView(LOGIN_VIEW));
        }
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理登录
     *
     * @return JSONObject
     */
    @RequestMapping(DO_LOGIN_URL)
    @ResponseBody
    @FormToken(remove = true)
    public Object login() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData result;
            PairUtils pair;
            String username = super.request.getParameter("username");
            String password = super.request.getParameter("password");
            String verifyCode = super.request.getParameter("verifyCode");
            // 验证数据
            result = this.validationData(username, password, verifyCode);
            if (result.isFail()) {
                if (!ProfileComponent.isDevProfile()) {
                    ShiroSessionUtils.setAttribute("isVerifyCode", "show");
                    result.set("isVerifyCode", "show");
                }
                // 生成 token 并放入 session 中
                super.initJSONObjectTokenValue(result);
                return result.toFastJson();
            }
            // 处理登录
            pair = adminUserService.login(username, DigestManageComponent.getSha512(password));
            if (String.valueOf(BusinessEnum.SUCCEED.getKey()).equals(pair.getLeft())) {
                // Shiro 验证
                boolean shiroError = this.validationShiroLogin(username, password);
                if (shiroError) {
                    result.setFail();
                    if (!ProfileComponent.isDevProfile()) {
                        ShiroSessionUtils.setAttribute("isVerifyCode", "show");
                        result.set("isVerifyCode", "show");
                    }
                    // 生成 isVerifyCode 并放入 session 中
                    super.initJSONObjectTokenValue(result);
                    result.set(GlobalMessage.MESSAGE, AdminGlobalMessage.LOGIN_FAILED);
                    return result.toFastJson();
                }

                /*验证通过*/
                // 去除验证码的限制
                if (this.validationVerifyCode()) {
                    ShiroSessionUtils.setAttribute("isVerifyCode", "hide");
                }
                // 利用 shiro 跳转到登录前的页面
                SavedRequest savedRequest = WebUtils.getSavedRequest(super.request);
                // 获取保存的URL
                String url;
                if (null == savedRequest || StringUtils.isBlank(savedRequest.getRequestUrl())) {
                    url = super.initUrl("main");
                } else {
                    String uri = savedRequest.getRequestUrl();
                    // 设置登录前的地址
                    url = this.initSavedRequest(uri);
                }
                result.setOk().set("url", url);
                return result.toFastJson();
            } else {
                if (!ProfileComponent.isDevProfile()) {
                    ShiroSessionUtils.setAttribute("isVerifyCode", "show");
                    return ResultData.fail().set(GlobalMessage.MESSAGE, pair.getRight()).set("isVerifyCode", "show")
                            .toFastJson();
                } else {
                    return ResultData.fail().set(GlobalMessage.MESSAGE, pair.getRight()).toFastJson();
                }
            }
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 退出登录
     *
     * @return ModelAndView
     */
    @RequestMapping(LOGOUT_URL)
    public ModelAndView logout() throws ControllerException {
        // shiro 管理的 session
        ShiroSessionUtils.removeAttribute(configConst.getAdminSessionName());
        ShiroSessionUtils.removeAttribute(configConst.getAdminMenuName());
        // shiro 销毁登录
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(super.initRedirectUrl(LOGIN_URL));
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检验是否需要验证码
     *
     * @return Boolean
     */
    private boolean validationVerifyCode() {
        String isVerifyCode = (String) ShiroSessionUtils.getAttribute("isVerifyCode");
        return "show".equals(isVerifyCode);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param username  String  用户名
     * @param password  String  密码
     * @param verifyCode    String  验证码
     *
     * @return ResultData
     */
    private ResultData validationData(final String username, final String password, final String verifyCode) {
        ResultData result;
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
            // 检验验证码
            if (!ProfileComponent.isDevProfile()) {
                if (this.validationVerifyCode()) {
                    if (null == verifyCode || !super.checkKaptcha(verifyCode)) {
                        result = ResultData.fail(GlobalMessage.MESSAGE, AdminGlobalMessage.VERIFY_CODE_FAILED);
                        return result;
                    }
                }
            }
            // 用户名有效性
            boolean usernameFlag = TextValidatorUtils
                    .isMatch(TextValidatorUtils.PATTERN_REGEX_CN_LETTER_NUM_UL_5TO18, username);
            if (!usernameFlag) {
                result = ResultData.fail(GlobalMessage.MESSAGE, AdminGlobalMessage.VERIFY_CODE_FAILED);
                return result;
            }
            // 密码有效性
            boolean passwordFlag = TextValidatorUtils
                    .isMatch(TextValidatorUtils.PATTERN_REGEX_LETTER_NUM_6TO18, password);
            if (!passwordFlag) {
                result = ResultData.fail(GlobalMessage.MESSAGE, AdminGlobalMessage.VERIFY_CODE_FAILED);
                return result;
            }
        } else {
            result = ResultData.fail(GlobalMessage.MESSAGE, AdminGlobalMessage.LOGIN_FAILED);
            return result;
        }
        return ResultData.ok();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * shiro 登录验证
     * @param username String
     * @param password String
     * @return boolean
     */
    private boolean validationShiroLogin(@NotNull final String username, @NotNull final String password) {
        // Shiro 验证
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        // shiro 异常
        boolean shiroError = false;
        try {
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException uae) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>用户[{}]进行登录验证...验证未通过,未知账户<<<<<", username);
            }
            shiroError = true;
        } catch (IncorrectCredentialsException ice) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>用户[{}]进行登录验证...验证未通过,错误的凭证<<<<<", username);
            }
            shiroError = true;
        } catch (LockedAccountException lae) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>用户[{}]进行登录验证...验证未通过,账户已锁定<<<<<", username);
            }
            shiroError = true;
        } catch (ExcessiveAttemptsException eae) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>用户[{}]进行登录验证...验证未通过,错误次数过多<<<<<", username);
            }
            shiroError = true;
        } catch (AuthenticationException ae) {
            if (logger.isErrorEnabled()) {
                // 通过处理 Shiro 的运行时 AuthenticationException 就可以控制用户登录失败或密码错误时的情景
                logger.error(">>>>>用户[{}]进行登录验证...验证未通过,堆栈轨迹如下<<<<<", username, ae.getCause());
            }
            shiroError = true;
        }
        // 验证通过
        if (subject.isAuthenticated()) {
            shiroError = false;
        }
        return shiroError;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置登录前的地址
     * @param uri String
     * @return String
     */
    private String initSavedRequest(@NotNull final String uri) {
        String url;
        String path = super.contextPath + "/";
        String nullStr = "null";
        String indexStr = "/index";
        if (StringUtils.isBlank(uri)) {
            url = super.initUrl("main");
        } else if (super.contextPath.equalsIgnoreCase(uri)) {
            url = super.initUrl("main");
        } else if (path.equalsIgnoreCase(uri)) {
            url = super.initUrl("main");
        } else if (uri.contains(configConst.getAdminLogoutUrlName())) {
            url = super.initUrl("main");
        } else if (nullStr.contains(uri)) {
            url = super.initUrl("main");
        } else if (uri.equals(contextPath)) {
            url = super.initUrl("main");
        } else if (uri.equals(configConst.getAdminLoginUrlName())) {
            url = super.initUrl("main");
        } else if (uri.startsWith(indexStr)) {
            url = super.initUrl("main");
        } else {
            url = super.initUrl(StringUtils.removeStart(uri, path));
        }
        return url;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LoginController class

/* End of file LoginController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/login/LoginController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
