/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.freemarker;


import cn.org.rapid_framework.freemarker.directive.BlockDirective;
import cn.org.rapid_framework.freemarker.directive.ExtendsDirective;
import cn.org.rapid_framework.freemarker.directive.OverrideDirective;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.blog.common.constant.ConfigConst;
import wang.encoding.mroot.blog.common.freemarker.modelex.I18nMethodModelEx;

import javax.annotation.PostConstruct;

/**
 * freemarker 配置
 *
 * @author ErYang
 */
@Component
public class FreemarkerComponent {

    private final ConfigConst configConst;
    private final freemarker.template.Configuration freemarkerConfiguration;


    @Autowired
    public FreemarkerComponent(ConfigConst configConst, freemarker.template.Configuration freemarkerConfiguration) {
        this.configConst = configConst;
        this.freemarkerConfiguration = freemarkerConfiguration;
    }

    // -------------------------------------------------------------------------------------------------

    @PostConstruct
    public void setSharedVariable() {
        // 国际化信息
        freemarkerConfiguration.setSharedVariable(configConst.getI18nName(), new I18nMethodModelEx());
        // 模板继承
        freemarkerConfiguration
                .setSharedVariable(configConst.getFreemarkerSharedVariableBlockName(), new BlockDirective());
        freemarkerConfiguration
                .setSharedVariable(configConst.getFreemarkerSharedVariableOverrideName(), new OverrideDirective());
        freemarkerConfiguration
                .setSharedVariable(configConst.getFreemarkerSharedVariableExtendsName(), new ExtendsDirective());

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FreemarkerComponent class

/* End of file FreemarkerComponent.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/freemarker/FreemarkerComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
