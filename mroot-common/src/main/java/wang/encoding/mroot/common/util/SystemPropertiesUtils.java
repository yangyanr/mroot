/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.number.NumberUtils;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 关于SystemProperties的工具类
 * 1. 统一风格的读取系统变量到各种数据类型，其中Boolean.readBoolean的风格不统一，Double则不支持，都进行了扩展.
 * 2. 简单的合并系统变量(-D)，环境变量 和默认值，以系统变量优先，在未引入Commons Config时使用.
 * 3. Properties 本质上是一个HashTable，
 * 每次读写都会加锁，所以不支持频繁的System.getProperty(name)来检查系统内容变化
 * 因此扩展了一个ListenableProperties,在其所关心的属性变化时进行通知.
 *
 * @author ErYang
 */
public class SystemPropertiesUtils {


    /**
     * 读取Boolean类型的系统变量，为空时返回null，代表未设置，而不是Boolean.getBoolean()的false.
     *
     * @param name String
     * @return Boolean
     */
    public static Boolean getBoolean(@Nullable final String name) {
        String stringResult = System.getProperty(name);
        return BooleanUtils.toBooleanObject(stringResult);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Boolean类型的系统变量，为空时返回默认值, 而不是Boolean.getBoolean()的false.
     *
     * @param name String
     * @param defaultValue Boolean
     * @return Boolean
     */
    public static Boolean getBoolean(@Nullable final String name, @NotNull final Boolean defaultValue) {
        String stringResult = System.getProperty(name);
        return BooleanUtils.toBooleanObject(stringResult, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取String类型的系统变量，为空时返回null.
     *
     * @param name String
     * @return String
     */
    public static String getString(@Nullable final String name) {
        return System.getProperty(name);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取String类型的系统变量，为空时返回null
     *
     * @param name String
     * @param defaultValue String
     * @return String
     */
    public static String getString(@Nullable final String name, @NotNull final String defaultValue) {
        return System.getProperty(name, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Integer类型的系统变量，为空时返回null.
     *
     * @param name String
     * @return Integer
     */
    public static Integer getInteger(@Nullable final String name) {
        return Integer.getInteger(name);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Integer类型的系统变量，为空时返回默认值
     *
     * @param name String
     * @param defaultValue Integer
     * @return Integer
     */
    public static Integer getInteger(@Nullable final String name, @NotNull final Integer defaultValue) {
        return Integer.getInteger(name, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Long类型的系统变量，为空时返回null
     *
     * @param name String
     * @return Long
     */
    public static Long getLong(@Nullable final String name) {
        return Long.getLong(name);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Integer类型的系统变量，为空时返回默认值
     *
     * @param name String
     * @param defaultValue Long
     * @return Long
     */
    public static Long getLong(@Nullable final String name, @NotNull final Long defaultValue) {
        return Long.getLong(name, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Double类型的系统变量，为空时返回null
     *
     * @param propertyName String
     * @return Double
     */
    public static Double getDouble(@Nullable final String propertyName) {
        return NumberUtils.toDoubleObject(System.getProperty(propertyName), null);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取Double类型的系统变量，为空时返回null.
     *
     * @param propertyName String
     * @param defaultValue Double
     * @return Double
     */
    public static Double getDouble(@Nullable final String propertyName, @NotNull final Double defaultValue) {
        Double propertyValue = NumberUtils.toDoubleObject(System.getProperty(propertyName), null);
        return null != propertyValue ? propertyValue : defaultValue;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 合并系统变量(-D)，环境变量 和默认值，以系统变量优先
     *
     * @param propertyName String
     * @param envName String
     * @param defaultValue String
     * @return String
     */
    public static String getString(@Nullable final String propertyName, @NotNull final String envName,
            @NotNull final String defaultValue) {
        checkEnvName(envName);
        String propertyValue = System.getProperty(propertyName);
        if (null != propertyValue) {
            return propertyValue;
        } else {
            propertyValue = System.getenv(envName);
            return null != propertyValue ? propertyValue : defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 合并系统变量(-D)，环境变量 和默认值，以系统变量优先
     *
     * @param propertyName String
     * @param envName String
     * @param defaultValue Integer
     * @return Integer
     */
    public static Integer getInteger(@Nullable final String propertyName, @NotNull final String envName,
            @NotNull final Integer defaultValue) {
        checkEnvName(envName);
        Integer propertyValue = NumberUtils.toIntObject(System.getProperty(propertyName), null);
        if (null != propertyValue) {
            return propertyValue;
        } else {
            propertyValue = NumberUtils.toIntObject(System.getenv(envName), null);
            return null != propertyValue ? propertyValue : defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 合并系统变量(-D)，环境变量 和默认值，以系统变量优先
     *
     * @param propertyName String
     * @param envName String
     * @param defaultValue Long
     * @return Long
     */
    public static Long getLong(@Nullable final String propertyName, @NotNull final String envName,
            @NotNull final Long defaultValue) {
        checkEnvName(envName);
        Long propertyValue = NumberUtils.toLongObject(System.getProperty(propertyName), null);
        if (null != propertyValue) {
            return propertyValue;
        } else {
            propertyValue = NumberUtils.toLongObject(System.getenv(envName), null);
            return null != propertyValue ? propertyValue : defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 合并系统变量(-D)，环境变量 和默认值，以系统变量优先
     *
     * @param propertyName String
     * @param envName String
     * @param defaultValue Double
     * @return Double
     */
    public static Double getDouble(@Nullable final String propertyName, @NotNull final String envName,
            @NotNull Double defaultValue) {
        checkEnvName(envName);
        Double propertyValue = NumberUtils.toDoubleObject(System.getProperty(propertyName), null);
        if (null != propertyValue) {
            return propertyValue;
        } else {
            propertyValue = NumberUtils.toDoubleObject(System.getenv(envName), null);
            return null != propertyValue ? propertyValue : defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 合并系统变量(-D)，环境变量 和默认值，以系统变量优先
     *
     * @param propertyName String
     * @param envName String
     * @param defaultValue Boolean
     * @return Boolean
     */
    public static Boolean getBoolean(@Nullable final String propertyName, @NotNull final String envName,
            @NotNull final Boolean defaultValue) {
        checkEnvName(envName);
        Boolean propertyValue = BooleanUtils.toBooleanObject(System.getProperty(propertyName), null);
        if (null != propertyValue) {
            return propertyValue;
        } else {
            propertyValue = BooleanUtils.toBooleanObject(System.getenv(envName), null);
            return null != propertyValue ? propertyValue : defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检查环境变量名不能有'.'，在linux下不支持
     *
     * @param envName String
     */
    private static void checkEnvName(@NotNull final String envName) {
        String dot = ".";
        if (null == envName || envName.contains(dot)) {
            throw new IllegalArgumentException(">>>>>>>>" + envName + "不能出现.<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------


    /**
     * Properties 本质上是一个HashTable，每次读写都会加锁，
     * 所以不支持频繁的System.getProperty(name)来检查系统内容变化 因此扩展了一个ListenableProperties,
     * 在其所关心的属性变化时进行通知
     *
     * @param listener AbstractPropertiesListener
     */
    public static synchronized void registerSystemPropertiesListener(final AbstractPropertiesListener listener) {
        Properties currentProperties = System.getProperties();

        // 将System的properties实现替换为ListenableProperties
        if (!(currentProperties instanceof ListenableProperties)) {
            ListenableProperties newProperties = new ListenableProperties(currentProperties);
            System.setProperties(newProperties);
            currentProperties = newProperties;
        }

        ((ListenableProperties) currentProperties).register(listener);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Properties 本质上是一个HashTable，每次读写都会加锁，
     * 所以不支持频繁的System.getProperty(name)来检查系统内容变化 因此扩展了Properties子类,
     * 在其所关心的属性变化时进行通知.
     */
    public static class ListenableProperties extends Properties {

        private static final long serialVersionUID = -8884794381458564759L;

        protected transient List<AbstractPropertiesListener> listeners = new CopyOnWriteArrayList<>();

        ListenableProperties(Properties properties) {
            super(properties);
        }

        // -------------------------------------------------------------------------------------------------

        public void register(final AbstractPropertiesListener listener) {
            listeners.add(listener);
        }

        // -------------------------------------------------------------------------------------------------

        @Override
        public synchronized Object setProperty(final String key, final String value) {
            Object result = put(key, value);
            for (AbstractPropertiesListener listener : listeners) {
                if (listener.propertyName.equals(key)) {
                    listener.onChange(key, value);
                }
            }
            return result;
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取所关心的Property变更的Listener基类
     */
    public abstract static class AbstractPropertiesListener {

        /**
         * 关心的Property
         */
        protected String propertyName;

        public AbstractPropertiesListener(final String propertyName) {
            this.propertyName = propertyName;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 改变后通知
         * @param propertyName String
         * @param value String
         */
        public abstract void onChange(final String propertyName, final String value);

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SystemPropertiesUtils class

/* End of file SystemPropertiesUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/SystemPropertiesUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
