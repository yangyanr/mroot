/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import org.apache.commons.collections.CollectionUtils;
import wang.encoding.mroot.common.annotation.NotNull;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.Set;

/**
 * Hibernate Validation 验证工具类
 *
 * @author ErYang
 */
public class HibernateValidationUtils {

    /**
     * 验证信息连接标识
     */
    public static final String CONNECT_CHAR = "@";
    /**
     * 得到 Validator 实例
     */
    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类
     *
     * @param t T 体类
     * @return String 验证信息
     */
    public static <T> String validateEntity(@NotNull final T t) {
        // 用于存储验证后的错误信息
        StringBuilder stringBuffer = new StringBuilder();
        Set<ConstraintViolation<T>> set = VALIDATOR.validate(t, Default.class);
        if (CollectionUtils.isNotEmpty(set)) {
            for (ConstraintViolation<T> constraintViolation : set) {
                stringBuffer.append(constraintViolation.getMessage());
                stringBuffer.append(CONNECT_CHAR);
            }
        }
        return stringBuffer.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类
     * 分组验证
     *
     * @param t T 实体类
     * @param var Class
     * @return String 验证信息
     */
    public static <T> String validateEntity(@NotNull final T t, @NotNull final Class... var) {
        // 用于存储验证后的错误信息
        StringBuilder stringBuffer = new StringBuilder();
        Set<ConstraintViolation<T>> set = VALIDATOR.validate(t, var);
        if (CollectionUtils.isNotEmpty(set)) {
            for (ConstraintViolation<T> constraintViolation : set) {
                stringBuffer.append(constraintViolation.getMessage());
                stringBuffer.append(CONNECT_CHAR);
            }
        }
        return stringBuffer.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类的某个字段
     *
     * @param t    T        实体类
     * @param propertyName String 字段名称
     * @return String 验证信息
     */
    public static <T> String validateProperty(@NotNull final T t, @NotNull final String propertyName) {
        // 用于存储验证后的错误信息
        StringBuilder stringBuffer = new StringBuilder();
        Set<ConstraintViolation<T>> set = VALIDATOR.validateProperty(t, propertyName, Default.class);
        if (CollectionUtils.isNotEmpty(set)) {
            for (ConstraintViolation<T> constraintViolation : set) {
                stringBuffer.append(constraintViolation.getMessage());
                stringBuffer.append(CONNECT_CHAR);
            }
        }
        return stringBuffer.toString();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HibernateValidationUtils class

/* End of file HibernateValidationUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/HibernateValidationUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

