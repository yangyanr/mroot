/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.define;

import java.util.HashMap;
import java.util.Map;


/**
 * 文件类型
 *
 * @author ErYang
 */
public class FileType {

    public static final String JPG = "JPG";
    public static final String JPEG = "JPEG";
    private static final String BMP = "BMP";
    private static final String PNG = "PNG";
    private static final String GIF = "GIF";

    private static final Map<String, String> TYPES = new HashMap<String, String>() {
        private static final long serialVersionUID = -1108012795040367651L;

        {
            put(FileType.JPG, ".jpg");
            put(FileType.JPEG, ".jpeg");
            put(FileType.BMP, ".bmp");
            put(FileType.PNG, ".png");
            put(FileType.GIF, ".gif");

        }
    };

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得后缀
     *
     * @param key String
     * @return String
     */
    public static String getSuffix(final String key) {
        return FileType.TYPES.get(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据给定的文件名 获取其后缀信息
     *
     * @param filename String
     * @return String
     */
    public static String getSuffixByFilename(String filename) {
        filename = filename.substring(filename.lastIndexOf(".")).toLowerCase();
        if (!TYPES.containsValue(filename.toLowerCase())) {
            return TYPES.get(FileType.JPG);
        }
        return filename;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FileType class

/* End of file FileType.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/define/FileType.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
