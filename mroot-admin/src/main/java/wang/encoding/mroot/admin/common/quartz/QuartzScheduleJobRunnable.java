/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.quartz;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;
import wang.encoding.mroot.common.component.SpringContextComponent;

import java.lang.reflect.Method;

/**
 * 执行定时任务
 *
 * @author ErYang
 */
@Slf4j
public class QuartzScheduleJobRunnable implements Runnable {


    /**
     * 类
     */
    private Object target;
    /**
     * 方法
     */
    private Method method;
    /**
     * 参数
     */
    private String params;

    // -------------------------------------------------------------------------------------------------

    /**
     * 构造方法
     *
     * @param beanName String
     * @param methodName String
     * @param params String
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    QuartzScheduleJobRunnable(final String beanName, final String methodName, final String params)
            throws NoSuchMethodException, SecurityException {
        this.target = SpringContextComponent.getBean(beanName);
        this.params = params;
        if (StringUtils.isNotBlank(params)) {
            this.method = target.getClass().getDeclaredMethod(methodName, String.class);
        } else {
            this.method = target.getClass().getDeclaredMethod(methodName);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 执行
     */
    @Override
    public void run() {
        try {
            ReflectionUtils.makeAccessible(method);
            if (StringUtils.isNotBlank(params)) {
                method.invoke(target, params);
            } else {
                method.invoke(target);
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>执行定时任务失败<<<<<<<<", e.getMessage());
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QuartzScheduleJobRunnable class

/* End of file QuartzScheduleJobRunnable.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/quartz/QuartzScheduleJobRunnable.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
