<#-- 页面用到的函数 -->

<#-- 默认值 -->
<#macro defaultStr str>
    <#if str??>${str}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateFormat date>
    <#if date??>${date?string("yyyy-MM-dd HH:mm:ss SSS")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro number2DateFormat date>
    <#if date??>${date?number_to_datetime}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 表格按钮 -->
<#macro tableOperate object>
    <@shiro.hasPermission name="${model}/edit or ${model}/view or ${model}/authorization or
 ${model}/run or ${model}/pause or ${model}/resume">

        <div class="dropdown ">
        <a href="javascript:" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
           data-toggle="dropdown"><i class="la la-ellipsis-h"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
        <@shiro.hasPermission name="${model}/edit">
            <a class="dropdown-item" href="${edit}/${object}">
        <i class="la la-edit"></i>${I18N("message.table.edit.btn")}</a>
        </@shiro.hasPermission>
        <@shiro.hasPermission name="${model}/view">
            <a class="dropdown-item" href="${view}/${object}">
        <i class="la la-eye"></i>${I18N("message.table.view.btn")}</a>
        </@shiro.hasPermission>
        <@shiro.hasPermission name="${model}/authorization">
            <a class="dropdown-item" href="${authorization}/${object}">
        <i class="la la-legal"></i>${I18N("message.table.authorization.btn")}</a>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="${model}/run">
            <a class="dropdown-item ajax_confirm_confirmation"
            data-url="${run}/${object}"
        href="javascript:">
        <i class="la la-play"></i>${I18N("message.table.run.btn")}
            </a>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="${model}/pause">
            <a class="dropdown-item ajax_confirm_confirmation"
            data-url="${pause}/${object}"
        href="javascript:">
        <i class="la la-pause"></i>${I18N("message.table.pause.btn")}
            </a>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="${model}/resume">
            <a class="dropdown-item ajax_confirm_confirmation"
            data-url="${resume}/${object}"
        href="javascript:">
        <i class="la la-play-circle"></i>${I18N("message.table.resume.btn")}
            </a>
        </@shiro.hasPermission>

        </div>
        </div>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="${model}/delete">
        <a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill ajax_confirm_confirmation"
        data-url="${delete}/${object}"
        href="javascript:">
        <i class="la la-trash"></i>
        </a>
    </@shiro.hasPermission>

    <@shiro.lacksPermission name="${model}/edit or ${model}/delete or ${model}/view or ${model}/authorization">
        ${I18N("message.default.content")}
    </@shiro.lacksPermission>

</#macro>

<#-- 表格按钮 回收站 -->
<#macro recycleBinTableOperate object>

    <@shiro.hasPermission name="${model}/recover">
        <a class="btn btn-primary m-btn m-btn--icon m-btn--pill ajax_confirm_confirmation"
        data-url="${recover}/${object}"
    href="javascript:">
    <i class="fa fa-medkit"></i>&nbsp;${I18N("message.table.recover.a")}
        </a>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="${model}/view">
        <a class="btn btn-info m-btn  m-btn--icon m-btn--pill"
        href="${view}/${object}">
    <i class="fa fa-eye"></i>&nbsp;${I18N("message.table.view.btn")}
        </a>
    </@shiro.hasPermission>

    <@shiro.lacksPermission name="${model}/recover or ${model}/view">
        ${I18N("message.default.content")}
    </@shiro.lacksPermission>

</#macro>

<#-- 表单按钮 -->
<#macro formOperate>

    <div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions">
    <div class="row">
    <div class="col-lg-9 ml-lg-auto">
    <button type="submit" class="btn btn-success">
    ${I18N("message.form.submit")}
    </button>
    <button type="reset" class="btn btn-secondary">
    ${I18N("message.form.reset")}
    </button>
    <#if refererUrl??>
        <a class="btn btn-info" href="${refererUrl}">
        ${I18N("message.form.back.btn")}
        </a>
    <#else>
    </#if>
    </div>
    </div>
    </div>
    </div>

</#macro>

<#-- 表单按钮 查看页面 -->
<#macro viewFormOperate>

    <div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions">
    <div class="row">
    <div class="col-lg-9 ml-lg-auto">
    <#if refererUrl??>
        <a class="btn btn-info" href="${refererUrl}">${I18N("message.form.back.btn")}</a>
    <#else>
        <a class="btn btn-info" href="${index}">${I18N("message.form.back.btn")}</a>
    </#if>
    </div>
    </div>
    </div>
    </div>

</#macro>

<#-- 表格头部 -->
<#macro tableTh th>
    <#list th as t>
        <th>${I18N('${t}')}</th>
    </#list>
</#macro>

<#-- 截取字符串 -->
<#macro subStr str length>
    <#if str??>
        <#if (length >= str?length)>
            <#if (length >= 20)>
                ${str?substring(0,20)}...
            <#else>
                ${str}
            </#if>
        <#else>
            ${str?substring(0,length)}...
        </#if>
    <#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 表单令牌和返回地址 -->
<#macro formTokenAndRefererUrl>
    <input id="formToken" type="hidden" name="formToken" value="${formToken}" autocomplete="off">
    <#if refererUrl??>
        <input name="refererUrl" type="hidden" value="${refererUrl}" autocomplete="off">
    <#else>
        <input name="refererUrl" type="hidden" value="${index}" autocomplete="off">
    </#if>
</#macro>

<#-- 金额 格式 1,000.00-->
<#macro moneyFormat money><#if money??>${money?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 金额 格式1000.00-->
<#macro moneyFormatUnComma money><#if money??>${money?string("0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字带0 -->
<#macro numberFormat number><#if number??>${number?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字 -->
<#macro numberFormatHasZero number><#if number??>${number?string(",##0")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 是/否 -->
<#macro boole value>
    <#if 1 == value>
        ${I18N("message.table.true.text")}
    <#else>
        ${I18N("message.table.false.text")}</#if>
</#macro>

<#-- i18n -->
<#macro i18nType i18n>
    <#if i18n??><#if ("zh" == language)>'zh-CN'<#else>'${i18n}'</#if><#else>'zh-CN'</#if>
</#macro>
