/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.requestlog.AdminRequestLogBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.util.Ip2RegionUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.RequestLogDO;
import wang.encoding.mroot.service.admin.system.AdminRequestLogService;
import wang.encoding.mroot.service.system.RequestLogService;
import wang.encoding.mroot.vo.admin.entity.system.requestlog.AdminRequestLogGetVO;


import java.math.BigInteger;
import java.util.List;


/**
 * 后台 请求日志 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminRequestLogServiceImpl implements AdminRequestLogService {


    private final RequestLogService requestLogService;

    @Autowired
    @Lazy
    public AdminRequestLogServiceImpl(RequestLogService requestLogService) {
        this.requestLogService = requestLogService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有数目
     *
     * @return int
     */
    @Override
    public int getCount() {
        RequestLogDO requestLogDO = new RequestLogDO();
        return requestLogService.countByT(requestLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminRequestLogGetVO
     */
    @Override
    public AdminRequestLogGetVO getById(@NotNull final BigInteger id) {
        RequestLogDO requestLogDO = requestLogService.getTById(id);
        if (null != requestLogDO) {
            return this.requestLogDO2AdminRequestLogGetVO(requestLogDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 请求日志
     *
     * @param adminRequestLogBO AdminRequestLogBO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminRequestLogBO adminRequestLogBO) {
        RequestLogDO requestLogDO = BeanMapperComponent.map(adminRequestLogBO, RequestLogDO.class);
        return requestLogService.saveByT(requestLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 请求日志
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean deleteById(@NotNull final BigInteger id) {
        return requestLogService.deleteById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 请求日志
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean deleteBatch(@NotNull final BigInteger[] idArray) {
        return requestLogService.deleteBatchById(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空记录
     *
     * @return boolean
     */
    @Override
    public boolean truncate() {
        return requestLogService.truncate();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRequestLogGetVO>
     * @param adminRequestLogGetVO AdminRequestLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRequestLogGetVO>
     */
    @Override
    public IPage<AdminRequestLogGetVO> list2page(@NotNull final Page<AdminRequestLogGetVO> pageAdmin,
            @NotNull final AdminRequestLogGetVO adminRequestLogGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<RequestLogDO> requestLogDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        RequestLogDO requestLogDO = BeanMapperComponent.map(adminRequestLogGetVO, RequestLogDO.class);
        requestLogService.list2page(requestLogDOPage, requestLogDO, orderByField, isAsc);
        if (null != requestLogDOPage.getRecords() && CollectionUtils.isNotEmpty(requestLogDOPage.getRecords())) {
            List<AdminRequestLogGetVO> list = ListUtils.newArrayList(requestLogDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (RequestLogDO requestLogInfoDO : requestLogDOPage.getRecords()) {
                AdminRequestLogGetVO requestLogGetVO = this.requestLogDO2AdminRequestLogGetVO(requestLogInfoDO);
                list.add(requestLogGetVO);
            }
        }
        pageAdmin.setTotal(requestLogDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size Integer
     * @return boolean
     */
    @Override
    public boolean delete2QuartzJob(final int size) {
        return requestLogService.delete2QuartzJob(size);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * RequestLogDO 转为 AdminRequestLogGetVO
     *
     * @param requestLogDO RequestLogDO
     * @return AdminRequestLogGetVO
     */
    private AdminRequestLogGetVO requestLogDO2AdminRequestLogGetVO(@NotNull final RequestLogDO requestLogDO) {
        AdminRequestLogGetVO adminRequestLogGetVO = BeanMapperComponent.map(requestLogDO, AdminRequestLogGetVO.class);
        if (null != adminRequestLogGetVO.getGmtCreateIp()) {
            adminRequestLogGetVO.setIp(IpUtils.intToIpv4String(adminRequestLogGetVO.getGmtCreateIp()));
            adminRequestLogGetVO.setIp2Region(Ip2RegionUtils.getRegionInfo(adminRequestLogGetVO.getIp()));
        }
        return adminRequestLogGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogServiceImpl class

/* End of file RequestLogServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/RequestLogServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
