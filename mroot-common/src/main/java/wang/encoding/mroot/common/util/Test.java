package wang.encoding.mroot.common.util;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.number.BigDecimalUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 测试类
 *
 * @author ErYang
 */
public class Test {


    /**
     * 等额本息计算获取还款方式为等额本息的每月偿还本金和利息
     *
     * 公式：每月偿还本息=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
     *
     * @param invest
     *            总借款额（贷款本金）
     * @param yearRate
     *            年利率
     * @param month
     *            还款总月数
     * @return 每月偿还本金和利息, 不四舍五入，直接截取小数点最后两位
     */
    public static double getPerMonthPrincipalInterest(double invest, double yearRate, int totalmonth) {
        double monthRate = yearRate / 12;
        BigDecimal monthIncome = new BigDecimal(invest)
                .multiply(new BigDecimal(monthRate * Math.pow(1 + monthRate, totalmonth)))
                .divide(new BigDecimal(Math.pow(1 + monthRate, totalmonth) - 1), 2, BigDecimal.ROUND_DOWN);
        return monthIncome.doubleValue();
    }

    /**
     * 等额本息计算获取还款方式为等额本息的每月偿还利息
     *
     * 公式：每月偿还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
     * @param invest
     *            总借款额（贷款本金）
     * @param yearRate
     *            年利率
     * @param month
     *            还款总月数
     * @return 每月偿还利息
     */
    public static Map<Integer, BigDecimal> getPerMonthInterest(double invest, double yearRate, int totalmonth) {
        Map<Integer, BigDecimal> map = new HashMap<Integer, BigDecimal>();
        double monthRate = yearRate / 12;
        BigDecimal monthInterest;
        for (int i = 1; i < totalmonth + 1; i++) {
            BigDecimal multiply = new BigDecimal(invest).multiply(new BigDecimal(monthRate));
            BigDecimal sub = new BigDecimal(Math.pow(1 + monthRate, totalmonth))
                    .subtract(new BigDecimal(Math.pow(1 + monthRate, i - 1)));
            monthInterest = multiply.multiply(sub)
                    .divide(new BigDecimal(Math.pow(1 + monthRate, totalmonth) - 1), 6, BigDecimal.ROUND_DOWN);
            monthInterest = monthInterest.setScale(2, BigDecimal.ROUND_DOWN);
            map.put(i, monthInterest);
        }
        return map;
    }

    /**
     * 等额本息计算获取还款方式为等额本息的每月偿还本金
     *
     * @param invest
     *            总借款额（贷款本金）
     * @param yearRate
     *            年利率
     * @param month
     *            还款总月数
     * @return 每月偿还本金
     */
    public static Map<Integer, BigDecimal> getPerMonthPrincipal(double invest, double yearRate, int totalmonth) {
        double monthRate = yearRate / 12;
        BigDecimal monthIncome = new BigDecimal(invest)
                .multiply(new BigDecimal(monthRate * Math.pow(1 + monthRate, totalmonth)))
                .divide(new BigDecimal(Math.pow(1 + monthRate, totalmonth) - 1), 2, BigDecimal.ROUND_DOWN);
        Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(invest, yearRate, totalmonth);
        Map<Integer, BigDecimal> mapPrincipal = new HashMap<Integer, BigDecimal>();

        for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
            mapPrincipal.put(entry.getKey(), monthIncome.subtract(entry.getValue()));
        }
        return mapPrincipal;
    }

    /**
     * 等额本息计算获取还款方式为等额本息的总利息
     *
     * @param invest
     *            总借款额（贷款本金）
     * @param yearRate
     *            年利率
     * @param month
     *            还款总月数
     * @return 总利息
     */
    public static double getInterestCount(double invest, double yearRate, int totalmonth) {
        BigDecimal count = new BigDecimal(0);
        Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(invest, yearRate, totalmonth);

        for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
            count = count.add(entry.getValue());
        }
        return count.doubleValue();
    }

    /**
     * 应还本金总和
     * @param invest
     *            总借款额（贷款本金）
     * @param yearRate
     *            年利率
     * @param month
     *            还款总月数
     * @return 应还本金总和
     */
    public static double getPrincipalInterestCount(double invest, double yearRate, int totalmonth) {
        double monthRate = yearRate / 12;
        BigDecimal perMonthInterest = new BigDecimal(invest)
                .multiply(new BigDecimal(monthRate * Math.pow(1 + monthRate, totalmonth)))
                .divide(new BigDecimal(Math.pow(1 + monthRate, totalmonth) - 1), 2, BigDecimal.ROUND_DOWN);
        BigDecimal count = perMonthInterest.multiply(new BigDecimal(totalmonth));
        count = count.setScale(2, BigDecimal.ROUND_DOWN);
        return count.doubleValue();
    }

    public static void main(String[] args) {
        String str = "123";
        System.out.println(str.length());

        Date dt = new Date();

        String year = String.format("%tY", dt);

        String mon = String.format("%tm", dt);

        String day = String.format("%td", dt);

        System.out.println(year);

        System.out.println(mon);

        System.out.println(day);

        // 图片
        String images = String.join(",", "sdfsfd");

        //println(getMd5("123456") + " " + getMd5("123456").length)
        // f51eb1dce6da0772473dfebdab77f26a57153732
        //println(getSha1("ANhui520") + " " + getSha1("ANhui520").length)

        //println(getSha256("123456123456") + " " + getSha256("123456123456").length)

        // 加密
        //var lStart = System.currentTimeMillis()
        // d2ca5d6f6d686ad7b8c59ef8fb5b7c91435f157ce7d2941f0baf28f9300460172941e34095f5748c7a158042b8ddf3c2461ef077bc8c64729c84069e663350a7
        //println(getSha512("anhui999") + " " + getSha512("anhui999").length)

        //var lUseTime = System.currentTimeMillis() - lStart
        //println("加密耗时：" + lUseTime + "毫秒")

        //println(getHmacMd5("123456") + " " + getHmacMd5("123456").length)
        //println(getHmacSha1("123456") + " " + getHmacSha1("123456").length)
        //println(getHmacSha256("123456") + " " + getHmacSha256("123456").length)
        //println(getHmacSha512("123456") + " " + getHmacSha512("123456").length)


        // 移除前缀
        String contextPath = "/";
        String url = "/admin";
        if (url.startsWith(contextPath)) {
            url = StringUtils.removeStart(url, contextPath);
        }

        System.out.println(url);

        url = "/admin/user/edit/1";
        int count = StringUtils.countMatches(url, "/");
        System.out.println(count);
        if (3 < count) {
            // 移除带/的地址 /admin/user/edit/* 变成 /admin/user/edit
            url = url.substring(0, url.lastIndexOf("/"));
        }

        System.out.println(url);

        String sss = "aaa_bbb_cc";
        System.out.println(wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(sss));
        String sss2 = "AAA_BBB_CCC";
        System.out.println(wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(sss2));
        String sss3 = "Aaa_BaB_CcC";
        System.out.println(wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(sss3));
        String sss4 = "aAA_bBB_cCC";
        System.out.println(wang.encoding.mroot.common.util.text.StringUtils.toCamelCase(sss4));

        String sssss = "ccc";
        System.out.println(StringUtils.uncapitalize(sssss));

        System.out.println("========================================================================");

        List<String> numSum = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            numSum.add("编号【" + (i + 1) + "】");
        }
        int num = numSum.size();
        // 请求的次数
        int batchIndex;
        int index = num % 70;
        if (0 == index) {
            batchIndex = num / 70;
        } else {
            batchIndex = num / 70 + 1;
        }

        System.out.println("numSum[" + numSum.size() + "] batchIndex:" + batchIndex);
        List<String> batchList;
        int i;
        int numI;
        for (i = 0; i < batchIndex; i++) {
            if (i == batchIndex - 1) {
                numI = num;
            } else {
                numI = (i + 1) * 70;
            }
            batchList = numSum.subList(i * 70, numI);
            for (int j = 0; j < batchList.size(); j++) {
                System.out.println(batchList.get(j) + "::" + j);
            }
        }

        System.out.println("========================================================================");

        double invest = 20000; // 本金
        int month = 12;
        double yearRate = 0.15; // 年利率
        double perMonthPrincipalInterest = getPerMonthPrincipalInterest(invest, yearRate, month);
        System.out.println("等额本息---每月还款本息：" + perMonthPrincipalInterest);
        Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(invest, yearRate, month);
        System.out.println("等额本息---每月还款利息：" + mapInterest);
        Map<Integer, BigDecimal> mapPrincipal = getPerMonthPrincipal(invest, yearRate, month);
        System.out.println("等额本息---每月还款本金：" + mapPrincipal);
        double count2 = getInterestCount(invest, yearRate, month);
        System.out.println("等额本息---总利息：" + count2);
        double principalInterestCount = getPrincipalInterestCount(invest, yearRate, month);
        System.out.println("等额本息---应还本息总和：" + principalInterestCount);

        System.out.println("========================================================================");
        BigDecimal invest2 = new BigDecimal(20000); // 本金
        BigDecimal yearRate2 = new BigDecimal(15); // 年利率

        BigDecimal perMonthPrincipalInterest2 = CalculateMoney2P2pUtils
                .monthMoney2MonthlyInterest(invest2, yearRate2, month);
        System.out.println("等额本息---每月还款本息：" + perMonthPrincipalInterest2);

        Map<Integer, BigDecimal> mapInterest2 = CalculateMoney2P2pUtils
                .mapInterest2MonthlyInterest(invest2, yearRate2, month);
        System.out.println("等额本息---每月还款利息：" + mapInterest2);

        Map<Integer, BigDecimal> mapPrincipal2 = CalculateMoney2P2pUtils
                .mapCapital2MonthlyInterest(invest2, yearRate2, month);
        System.out.println("等额本息---每月还款本金：" + mapPrincipal2);

        BigDecimal count3 = CalculateMoney2P2pUtils.totalInterest2MonthlyInterest(invest2, yearRate2, month);
        System.out.println("等额本息---总利息：" + count3);

        BigDecimal principalInterestCount2 = CalculateMoney2P2pUtils
                .totalMoney2MonthlyInterest(invest2, yearRate2, month);
        System.out.println("等额本息---应还本息总和：" + principalInterestCount2);


        System.out.println("一次性还款：" + CalculateMoney2P2pUtils.interest2Day360(invest2, yearRate2, month * 30));

        System.out.println("月还息：" + CalculateMoney2P2pUtils.interest2Monthly(invest2, yearRate2));
        System.out.println("月还息：" + CalculateMoney2P2pUtils.totalInterest2Monthly(invest2, yearRate2, month));

        System.out.println("========================================================================");

        List<String> list = new ArrayList<>();
        System.out.println("List为空：" + CollectionUtils.isEmpty(list));

        System.out.println("========================================================================");

        String filterFields = "  ";
        System.out.println("字符串为空：" + StringUtils.isBlank(filterFields));

        System.out.println("========================================================================");

        String name = "-23123";
        System.out.println("字符串是否为数字：" + StringUtils.isNumeric(name));
        name = "23123.1";
        System.out.println("23123.1字符串是否为数字：" + StringUtils.isNumeric(name));
        name = "23123";
        System.out.println("23123字符串是否为数字：" + NumberUtils.isCreatable(name));
        name = "023123.0";
        System.out.println("023123.0字符串是否为数字：" + NumberUtils.isCreatable(name));
        name = "-23123";
        System.out.println("-23123字符串是否为数字：" + NumberUtils.isCreatable(name));
        name = "-2312331.0";
        System.out.println("-2312331.0字符串是否为数字：" + NumberUtils.isCreatable(name));
        name = "-002312331.01";
        System.out.println(
                "-002312331.01字符串是否为数字：" + NumberUtils.isCreatable(name) + ":" + NumberUtils.toScaledBigDecimal(name)
                        + ":" + BigDecimalUtils.gt(NumberUtils.toScaledBigDecimal(name),BigDecimal.ZERO) );
    }

}
