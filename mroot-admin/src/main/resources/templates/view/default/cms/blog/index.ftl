<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">

    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.table.head.title")}
    </h3>
    </div>
    </div>

    </div>

    <div class="m-portlet__body">

    <div class="m-content">

    <div class="row">

    <#if page.records??  && (0 < page.records?size)>
        <#list page.records as item>
            <div class="col-xl-6">
            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
            <div class="m-portlet__head m-portlet__head--fit">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-action">
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
            <div class="m-widget19">
            <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides"
            style="min-height: 286px">
            <#if item.cover??>
                <img src="${item.cover}" alt="${item.title}">
            <#else>
                <img src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/logos/logo-2.png" alt="${item.title}">
            </#if>
            <h3 class="m-widget19__title m--font-light">
            ${item.title}
            </h3>
        <div class="m-widget19__shadow"></div>
            </div>
            <div class="m-widget19__content">
            <div class="m-widget19__header">
            <div class="m-widget19__stats">
            <span class="m-widget19__number m--font-brand">
            ${item.pageView}
            </span>
            <span class="m-widget19__comment">
            ${I18N("message.cms.blog.list.table.pageView")}
            </span>
            </div>
            </div>
            <div class="m-widget19__body">
            ${item.description}
            </div>
            </div>
            <div class="m-widget19__action">
        <a class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom"
        href="${view}/${item.id}">
            ${I18N("message.cms.blog.list.table.details")}
            </a>
            </div>
            </div>
            </div>
            </div>
            </div>
        </#list>
    <#else>
        <div class="col-xl-12">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
        <div class="row">
        <div class="col-xl-4 offset-xl-4">
        <p class="m--align-center">${I18N("message.table.empty.content")}</p></div>
        </div>
        </div>
        </div>
        </div>
    </#if>
    <div class="col-xl-12">
        <div class="m-datatable m-datatable--default">
            <div id="paginate"
                 class="m-datatable__pager m-datatable--paging-loaded clearfix"></div>
        </div>
    </div>

    </div>

    </div>
    </div>

    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/index.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        // 分页
        Table.pagination({
            url: '${index}',
            totalRow: '${page.total}',
            pageSize: '${page.size}',
            pageNumber: '${page.current}',
            params: function () {
                return {
                    <#if title??>title: '${title}'</#if>
                };
            }
        });

    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
