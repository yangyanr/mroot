/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.common.exception;


import wang.encoding.mroot.common.annotation.NotNull;

/**
 * Controller 异常
 *
 * @author ErYang
 */
public class ControllerException extends BaseException {

    private static final long serialVersionUID = 4395334385572658818L;

    /**
     * 异常信息
     */
    public ControllerException() {
        super();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 异常信息
     *
     * @param message String
     */
    public ControllerException(@NotNull final String message) {
        super(message);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 异常信息
     *
     * @param message String
     * @param cause   Throwable
     */
    public ControllerException(@NotNull final String message,
                               @NotNull final Throwable cause) {
        super(message, cause);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ControllerException class

/* End of file ControllerException.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/exception/ControllerException.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
