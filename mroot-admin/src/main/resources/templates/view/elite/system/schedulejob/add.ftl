<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_schedule-job-form" action="${save}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sole")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sole"
                                       placeholder="${I18N("message.form.sole.text")}"
                                       value="${schedulejob.sole}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.sole.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title"
                                       placeholder="${I18N("message.form.title.text")}"
                                       value="${schedulejob.title}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.title.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.scheduleJob.form.beanName")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="beanName"
                                       placeholder="${I18N("message.system.scheduleJob.form.beanName.text")}"
                                       value="${schedulejob.beanName}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.scheduleJob.form.beanName.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.scheduleJob.form.methodName")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="methodName"
                                       placeholder="${I18N("message.system.scheduleJob.form.methodName.text")}"
                                       value="${schedulejob.methodName}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.scheduleJob.form.methodName.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.scheduleJob.form.params")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="params"
                                       placeholder="${I18N("message.system.scheduleJob.form.params.text")}"
                                       value="${scheduleJob.params}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.scheduleJob.form.params.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.scheduleJob.form.cronExpression")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="cronExpression"
                                       placeholder="${I18N("message.system.scheduleJob.form.cronExpression.text")}"
                                       value="${schedulejob.cronExpression}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.scheduleJob.form.cronExpression.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.remark")}</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="5" name="remark"
                                          placeholder="${I18N("message.form.remark.text")}">${schedulejob.remark}</textarea>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.remark.text.help")}</small></span>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/schedulejob/schedulejobjs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteFormValidation.formValidationScheduleJob();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
