/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.common.util;


import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

/**
 * Boolean 工具类
 * 1. 从String(true/false, yes/no)，转换为Boolean或boolean
 * 2. 逻辑运算：取反，多个boolean的and,or 计算
 * 封装 org.apache.commons.lang3.BooleanUtils
 *
 * @author ErYang
 */
public class BooleanUtils {


    /**
     * 使用标准JDK，只分析是否忽略大小写的"true", str为空时返回false
     *
     * @param str String
     * @return boolean
     */
    public static boolean toBoolean(@Nullable final String str) {
        return Boolean.parseBoolean(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 使用标准JDK，只分析是否忽略大小写的"true", str为空时返回false
     *
     * @param str String
     * @return Boolean
     */
    public static Boolean toBooleanObject(@Nullable final String str) {
        return null != str ? Boolean.valueOf(str) : null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 使用标准JDK，只分析是否忽略大小写的"true", str为空时返回defaultValue
     *
     * @param str          String
     * @param defaultValue Boolean
     * @return Boolean
     */
    public static Boolean toBooleanObject(@Nullable final String str, @NotNull final Boolean defaultValue) {
        return null != str ? Boolean.valueOf(str) : defaultValue;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 支持true/false, on/off, y/n, yes/no的转换, str为空或无法分析时返回null
     *
     * @param str String
     * @return Boolean
     */
    public static Boolean parseGeneralString(@Nullable final String str) {
        return org.apache.commons.lang3.BooleanUtils.toBooleanObject(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 支持true/false,on/off, y/n, yes/no的转换, str为空或无法分析时返回defaultValue
     *
     * @param str          String
     * @param defaultValue Boolean
     * @return Boolean
     */
    public static Boolean parseGeneralString(@Nullable final String str, @NotNull final Boolean defaultValue) {
        return org.apache.commons.lang3.BooleanUtils.
                toBooleanDefaultIfNull(org.apache.commons.lang3.BooleanUtils.toBooleanObject(str),
                        defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 取反
     *
     * @param bool boolean
     * @return boolean
     */
    public static boolean negate(@NotNull final boolean bool) {
        return !bool;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 取反
     *
     * @param bool Boolean
     * @return Boolean
     */
    public static Boolean negate(@Nullable final Boolean bool) {
        return org.apache.commons.lang3.BooleanUtils.negate(bool);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 多个值的and
     *
     * @param array boolean
     * @return boolean
     */
    public static boolean and(@NotNull final boolean... array) {
        return org.apache.commons.lang3.BooleanUtils.and(array);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 多个值的or
     *
     * @param array boolean
     * @return boolean
     */
    public static boolean or(@NotNull final boolean... array) {
        return org.apache.commons.lang3.BooleanUtils.or(array);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BooleanUtils class

/* End of file BooleanUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/BooleanUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
