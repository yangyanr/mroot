/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.bo.admin.entity.system.user.AdminUserBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.PairUtils;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import java.math.BigInteger;


/**
 * 用户后台 Service
 *
 * @author ErYang
 */
public interface AdminUserService {


    /**
     * 得到所有数目
     *
     * @return int
     */
    int getCount();

    // -------------------------------------------------------------------------------------------------

    /**
     * 登录
     *
     * @param username String 用户名
     * @param password String 密码
     * @return ResultData
     */
    PairUtils login(@NotNull final String username, @NotNull final String password);

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过用户和密码查询
     *
     * @param username String 用户名
     * @param password String 密码
     * @return AdminUserBO
     */
    AdminUserBO getByUsernameAndPassword(@NotNull final String username, @NotNull final String password);

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminUserGetVO
     */
    AdminUserGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户名查询 AdminUserGetVO
     *
     * @param username String 用户名
     * @return AdminUserGetVO
     */
    AdminUserGetVO getByUsername(@NotNull final String username);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据昵称查询 AdminUserGetVO
     *
     * @param nickName String 昵称
     * @return AdminUserGetVO
     */
    AdminUserGetVO getByNickName(@NotNull final String nickName);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据电子邮箱查询 AdminUserGetVO
     *
     * @param email String 电子邮箱
     * @return AdminUserGetVO
     */
    AdminUserGetVO getByEmail(@NotNull final String email);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据手机号码查询 AdminUserGetVO
     *
     * @param phone String 手机号码
     * @return AdminUserGetVO
     */
    AdminUserGetVO getByPhone(@NotNull final String phone);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增用户
     *
     * @param userVO AdminUserGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminUserGetVO userVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新用户
     *
     * @param userVO AdminUserGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminUserGetVO userVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除用户(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复用户(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean recoverById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminUserGetVO>
     * @param adminUserGetVO AdminUserGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminUserGetVO>
     */
    IPage<AdminUserGetVO> list2page(@NotNull final Page<AdminUserGetVO> pageAdmin,
            @NotNull final AdminUserGetVO adminUserGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param userVO AdminUserGetVO
     *
     * @return String
     */
    String validationUser(@NotNull final AdminUserGetVO userVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminUserGetVO 缓存
     *
     * @param id BigInteger
     */
    void removeCacheById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminUserService class

/* End of file AdminUserService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminUserService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
