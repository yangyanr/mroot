var SnippetLogin = function () {

    /**
     * 错误信息
     * @param form
     * @param type
     * @param msg
     */
    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible"\
         role="alert"><button type="button" class="close" data-dismiss="alert">\
         </button><span></span></div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.find('span').html(msg);
    };

    /**
     * 错误信息
     */
    var hideErrorMsg = function () {
        $('.m-alert').hide();
        $('.m-alert').find('span').html('');
    };

    /**
     * 处理登录
     */
    var handleSignInFormSubmit = function () {

        $('#m_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true,
                        cnLetterNumUl5To18: true
                    },
                    password: {
                        required: true,
                        letterNum6To18: true
                    }
                },
                messages: {
                    username: {
                        required: LoginValidation.getUsernameRequired(),
                        cnLetterNumUl5To18: LoginValidation.getUsernameCnLetterNumUl5To18()
                    },
                    password: {
                        required: LoginValidation.getPasswordRequired(),
                        letterNum6To18: LoginValidation.getPasswordLetterNum6To18()

                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            var isVerifyCode = $('#isVerifyCode').val();
            if ("show" !== isVerifyCode) {
                //表单提交
                var url = form.get(0).action;

                form.ajaxSubmit({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: form.serialize(),
                    success: function (result) {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        $('#formToken').val(result.formToken);
                        // 刷新验证码
                        //$('.verifyImg').click();
                        if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                            hideErrorMsg();
                            if (result.url) {
                                window.location.href = result.url;
                            }
                        } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                            $('#isVerifyCode').val(result.isVerifyCode);
                            showErrorMsg(form, 'danger', result.message);
                        } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                            $('#isVerifyCode').val(result.isVerifyCode);
                            showErrorMsg(form, 'warning', result.message);
                        } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                            $('#isVerifyCode').val(result.isVerifyCode);
                            showErrorMsg(form, 'brand', result.message);
                        } else {
                            $('#isVerifyCode').val(result.isVerifyCode);
                            showErrorMsg(form, 'primary', result.message);
                        }
                    },
                    error: function () {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', AlertMessage.getMessageAlertNetworkError());
                    }
                });
            } else {
                hideErrorMsg();
                // 刷新验证码
                $('.verifyImg').click();
                $('#verify_code').val('');
                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                $('#verify_code_modal').modal({backdrop: 'static', keyboard: false});
                $('#verify_code_modal').modal('show');
            }

        });

        $('#m_verify_code_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $('#login_form');

            $('#verify_code_modal').modal('hide');
            $('#verifyCode').val($('#verify_code').val());

            $('#verify_code').val('');


            form.validate({
                rules: {
                    username: {
                        required: true,
                        cnLetterNumUl5To18: true
                    },
                    password: {
                        required: true,
                        letterNum6To18: true
                    },
                    verifyCode: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: LoginValidation.getUsernameRequired(),
                        cnLetterNumUl5To18: LoginValidation.getUsernameCnLetterNumUl5To18()
                    },
                    password: {
                        required: LoginValidation.getPasswordRequired(),
                        letterNum6To18: LoginValidation.getPasswordLetterNum6To18()

                    },
                    verifyCode: {
                        required: LoginValidation.getVerifyCodeRequired()
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            //表单提交
            var url = form.get(0).action;

            form.ajaxSubmit({
                url: url,
                method: 'POST',
                dataType: 'JSON',
                data: form.serialize(),
                success: function (result) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    $('#formToken').val(result.formToken);
                    // 刷新验证码
                    $('.verifyImg').click();
                    if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                        hideErrorMsg();
                        if (result.url) {
                            window.location.href = result.url;
                        }
                    } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                        $('#isVerifyCode').val(result.isVerifyCode);
                        showErrorMsg(form, 'danger', result.message);
                    } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                        $('#isVerifyCode').val(result.isVerifyCode);
                        showErrorMsg(form, 'warning', result.message);
                    } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                        $('#isVerifyCode').val(result.isVerifyCode);
                        showErrorMsg(form, 'brand', result.message);
                    } else {
                        $('#isVerifyCode').val(result.isVerifyCode);
                        showErrorMsg(form, 'primary', result.message);
                    }
                },
                error: function () {
                    showErrorMsg(form, 'danger', AlertMessage.getMessageAlertNetworkError());
                }
            });

        });

    };

    var handleVerifyImg = function () {
        //刷新验证码
        var verifyImg = $('.verifyImg').attr('src');
        $('.verifyImg').click(function () {
            if (verifyImg.indexOf('?') > 0) {
                $('.verifyImg').attr('src', verifyImg + '&random=' + Math.random());
            } else {
                $('.verifyImg').attr('src', verifyImg.replace(/\?.*$/, '') + '?' + Math.random());
            }
        });

    };

    return {
        init: function () {
            handleSignInFormSubmit();
            handleVerifyImg();
        }

    };
}();

jQuery(document).ready(function () {
    SnippetLogin.init();
});
