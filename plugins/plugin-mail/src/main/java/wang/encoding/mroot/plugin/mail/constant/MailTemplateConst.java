/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.mail.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import static wang.encoding.mroot.plugin.mail.constant.MailTemplateConst.MAIL_TEMPLATE_PREFIX;

/**
 * 邮件模版配置文件
 *
 * @author ErYang
 */
@Component
@PropertySource(value = {"classpath:mail-template.properties"})
@ConfigurationProperties(prefix = MAIL_TEMPLATE_PREFIX)
@EnableCaching
@Getter
@Setter
public class MailTemplateConst {

    /**
     * mail 模版 配置前缀
     */
    public static final String MAIL_TEMPLATE_PREFIX = "mail.template";

    /**
     * 接收邮箱地址
     */
    private String toMail;
    /**
     * 测试邮件 标题
     */
    private String testTitle;
    /**
     * 测试邮件 内容
     */
    private String testContent;
    /**
     * 系统监控邮件 标题
     */
    private String systemMonitoringTitle;
    /**
     * 系统监控邮件 内容
     */
    private String systemMonitoringContent;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MailConst class

/* End of file MailConst.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/mail/constant/MailConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
