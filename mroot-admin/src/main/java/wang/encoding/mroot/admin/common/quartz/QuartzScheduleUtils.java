/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.quartz;


import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.ExceptionUtils;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;


/**
 * Quartz Scheduler 工具类
 *
 * @author ErYang
 */
@Slf4j
public class QuartzScheduleUtils {

    /**
     * 任务组 key
     */
    private static final String JOB_GROUP_KEY = "MROOT_GROUP_JOB";
    /**
     * 任务 key
     */
    private static final String JOB_KEY = "MROOT_JOB";
    /**
     * 触发器组 key
     */
    private static final String TRIGGER_GROUP_KEY = "MROOT_GROUP_TRIGGER";
    /**
     * 触发器 key
     */
    private static final String TRIGGER_KEY = "MROOT_TRIGGER";


    /**
     * 根据 scheduler 和 scheduleJob 新增定时任务
     *
     * @param scheduler   scheduler
     * @param scheduleJobVO AdminScheduleJobGetVO
     */
    public static void addScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            // 构建 job 信息
            JobDetail jobDetail = JobBuilder.newJob(QuartzScheduleJob.class)
                    .withIdentity(QuartzScheduleUtils.getJobKey(scheduleJobVO)).build();

            // 表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJobVO.getCronExpression());

            // 按新的 cronExpression 表达式构建一个新的 trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(QuartzScheduleUtils.getTriggerKey())
                    .withSchedule(scheduleBuilder).build();

            // 放入参数，运行时的方法可以获取
            jobDetail.getJobDataMap().put("scheduleJob", scheduleJobVO);
            scheduler.scheduleJob(jobDetail, trigger);

            // 暂停任务
            if (StateEnum.NORMAL.getKey() != scheduleJobVO.getState()) {
                QuartzScheduleUtils.pauseScheduleJob(scheduler, scheduleJobVO);
            }

        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>创建定时任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 和 AdminScheduleJobGetVO 修改定时任务
     *
     * @param scheduler   scheduler
     * @param scheduleJobVO AdminScheduleJobGetVO
     */
    public static void editScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            // 获取触发器
            TriggerKey triggerKey = QuartzScheduleUtils.getTriggerKey();

            // 表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJobVO.getCronExpression());
            // 获得 trigger
            CronTrigger trigger = QuartzScheduleUtils.getCronTrigger(scheduler);
            // 按新的 cronExpression 表达式重新构建 trigger
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();

            // 放入参数，运行时的方法可以获取
            trigger.getJobDataMap().put("scheduleJob", scheduleJobVO);
            scheduler.rescheduleJob(triggerKey, trigger);
            // 暂停任务
            if (StateEnum.NORMAL.getKey() != scheduleJobVO.getState()) {
                QuartzScheduleUtils.pauseScheduleJob(scheduler, scheduleJobVO);
            }

        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>更新定时任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 和 scheduleJob 立即执行任务
     *
     * @param scheduler   scheduler
     * @param scheduleJobVO AdminScheduleJobGetVO 定时任务
     */
    public static void runScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            // 参数
            JobDataMap dataMap = new JobDataMap();
            dataMap.put("scheduleJob", scheduleJobVO);
            scheduler.triggerJob(QuartzScheduleUtils.getJobKey(scheduleJobVO), dataMap);
        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>立即执行任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 和 scheduleJob 暂停任务
     *
     * @param scheduler scheduler
     * @param scheduleJobVO AdminScheduleJobGetVO
     */
    public static void pauseScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            scheduler.pauseJob(QuartzScheduleUtils.getJobKey(scheduleJobVO));
        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>暂停任务任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 和 scheduleJob 恢复任务
     *
     * @param scheduler scheduler
     * @param scheduleJobVO AdminScheduleJobGetVO
     */
    public static void resumeScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            scheduler.resumeJob(QuartzScheduleUtils.getJobKey(scheduleJobVO));
        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>恢复定时任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 和 id 删除定时任务
     *
     * @param scheduler scheduler
     * @param scheduleJobVO        AdminScheduleJobGetVO
     */
    public static void deleteScheduleJob(@NotNull final Scheduler scheduler,
            @NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        try {
            // 停止触发器
            scheduler.pauseTrigger(QuartzScheduleUtils.getTriggerKey());
            // 移除触发器
            scheduler.unscheduleJob(QuartzScheduleUtils.getTriggerKey());
            scheduler.deleteJob(QuartzScheduleUtils.getJobKey(scheduleJobVO));
        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>删除定时任务失败<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 scheduler 获取表达式触发器
     *
     * @param scheduler Scheduler
     * @return CronTrigger
     */
    public static CronTrigger getCronTrigger(@NotNull final Scheduler scheduler) {
        CronTrigger cronTrigger;
        try {
            cronTrigger = (CronTrigger) scheduler.getTrigger(QuartzScheduleUtils.getTriggerKey());
            return cronTrigger;
        } catch (SchedulerException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>获取定时任务CronTrigger出现异常<<<<<<<<", e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据定时任务获取触发器 JobKey
     *
     * @param scheduleJobVO AdminScheduleJobGetVO
     * @return JobKey
     */
    private static JobKey getJobKey(@NotNull final AdminScheduleJobGetVO scheduleJobVO) {
        return JobKey.jobKey(JOB_KEY + scheduleJobVO.getId(), JOB_GROUP_KEY);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取触发器 TriggerKey
     *
     *
     * @return TriggerKey
     */
    private static TriggerKey getTriggerKey() {
        return TriggerKey.triggerKey(TRIGGER_KEY, TRIGGER_GROUP_KEY);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QuartzScheduleUtil class

/* End of file QuartzScheduleUtil.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/quartz/QuartzScheduleUtil.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
