/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.bo.admin.entity.system.rule.AdminRuleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;

import java.math.BigInteger;
import java.util.List;
import java.util.TreeSet;

/**
 * 权限后台 Service
 *
 * @author ErYang
 */
public interface AdminRuleService {

    /**
     * 根据角色 ID 查询权限
     *
     * @param roleId BigInteger 角色id
     * @return TreeSet<AdminRuleBO>
     */
    TreeSet<AdminRuleBO> listByRoleId(@NotNull final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     *
     * @return List
     */
    List<AdminRuleGetVO> listPidGt0AndTypeLt4();

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 地址查询权限
     *
     * @param id BigInteger
     * @return AdminRuleGetVO
     */
    AdminRuleGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 url 地址查询权限
     *
     * @param url String url地址
     * @return AdminRuleGetVO
     */
    AdminRuleGetVO getByUrl(@NotNull final String url);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminRuleGetVO
     *
     * @param title String 名称
     * @return AdminRuleGetVO
     */
    AdminRuleGetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 权限
     *
     * @param ruleGetVO AdminRuleGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminRuleGetVO ruleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 权限
     *
     * @param ruleGetVO AdminRuleGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminRuleGetVO ruleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 权限 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRuleGetVO>
     * @param adminRuleGetVO AdminRuleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRuleGetVO>
     */
    IPage<AdminRuleGetVO> list2page(@NotNull final Page<AdminRuleGetVO> pageAdmin,
            @NotNull final AdminRuleGetVO adminRuleGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param ruleGetVO AdminRuleGetVO
     *
     * @return String
     */
    String validationRule(@NotNull final AdminRuleGetVO ruleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminRuleGetVO 缓存
     *
     * @param id BigInteger
     */
    void removeCacheById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    int getMax2Sort();

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0 权限集合
     *
     * @return List
     */
    List<AdminRuleGetVO> listPidGt0();

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询 pid 大于 0 权限集合
     *
     * @param roleId BigInteger 角色id
     *
     * @return List
     */
    List<AdminRuleGetVO> listByRoleIdAndPidGt0(@NotNull final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询权限 id
     *
     * @param roleId BigInteger 角色id
     * @return String id 集合字符串
     */
    String listIdByRoleId2String(@NotNull final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量新增 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    int saveBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId, @NotNull final BigInteger[] ruleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 和 权限id 批量删除 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    void removeBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId, @NotNull final BigInteger[] ruleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 批量删除 角色-权限 表
     *
     * @param roleIdArray BigInteger[]
     *
     * @return int
     */
    void removeByRoleIdArray(@NotNull final BigInteger[] roleIdArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRuleService class

/* End of file AdminRuleService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminRuleService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
