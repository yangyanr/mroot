/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.loggingevent;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.system.AdminLoggingEventService;
import wang.encoding.mroot.vo.admin.entity.system.loggingevent.AdminLoggingEventGetVO;

import java.math.BigInteger;


/**
 * 后台 logback日志 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/system/loggingevent")
public class LoggingEventController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/loggingevent";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/loggingevent";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "loggingEvent";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;

    private final AdminLoggingEventService adminLoggingEventService;

    @Autowired
    public LoggingEventController(AdminLoggingEventService adminLoggingEventService) {
        this.adminLoggingEventService = adminLoggingEventService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_LOGGING_LOGGINGEVENT_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminLoggingEventGetVO adminLoggingEventGetVO = new AdminLoggingEventGetVO();
        String levelString = super.request.getParameter("levelString");
        if (StringUtils.isNotBlank(levelString)) {
            adminLoggingEventGetVO.setLevelString(levelString);
        }

        Page<AdminLoggingEventGetVO> pageInt = new Page<>();
        IPage<AdminLoggingEventGetVO> pageAdmin = adminLoggingEventService
                .list2page(super.initPage(pageInt), adminLoggingEventGetVO, AdminLoggingEventGetVO.EVENT_ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_LOGGING_LOGGINGEVENT_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminLoggingEventGetVO loggingEventGetVO = adminLoggingEventService.getById(idValue);
        if (null == loggingEventGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, loggingEventGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LoggingEventController class

/* End of file LoggingEventController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/loggingevent/LoggingEventController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
