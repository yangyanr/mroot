/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.cms.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.cms.ArticleContentDO;
import wang.encoding.mroot.service.blog.cms.BlogArticleContentService;
import wang.encoding.mroot.service.cms.ArticleContentService;
import wang.encoding.mroot.vo.blog.entity.cms.articlecontent.BlogArticleContentGetVO;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * 博客 文章内容 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class BlogArticleContentServiceImpl implements BlogArticleContentService {


    private final ArticleContentService articleContentService;

    @Autowired
    @Lazy
    public BlogArticleContentServiceImpl(ArticleContentService articleContentService) {
        this.articleContentService = articleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return BlogArticleContentGetVO
     */
    @Override
    public BlogArticleContentGetVO getById(@NotNull final BigInteger id) {
        ArticleContentDO articleContentDO = articleContentService.getTById(id);
        if (null != articleContentDO) {
            return this.articleContentDO2BlogArticleContentGetVO(articleContentDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  BlogArticleContentGetVO 列表
     * @param articleContentGetVO BlogArticleContentGetVO
     * @return List
     */
    @Override
    public List<BlogArticleContentGetVO> list(@NotNull final BlogArticleContentGetVO articleContentGetVO) {
        ArticleContentDO articleContentDO = BeanMapperComponent.map(articleContentGetVO, ArticleContentDO.class);
        List<ArticleContentDO> list = articleContentService.listByT(articleContentDO);
        if (ListUtils.isNotEmpty(list)) {
            List<BlogArticleContentGetVO> listVO = new ArrayList<>();
            for (ArticleContentDO articleContentInfoDO : list) {
                BlogArticleContentGetVO articleContentGetVOInfo = this
                        .articleContentDO2BlogArticleContentGetVO(articleContentInfoDO);
                listVO.add(articleContentGetVOInfo);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ArticleContentDO 转为 AdminArticleContentGetVO
     *
     * @param articleContentDO ArticleContentDO
     * @return AdminArticleContentGetVO
     */
    private BlogArticleContentGetVO articleContentDO2BlogArticleContentGetVO(
            @NotNull final ArticleContentDO articleContentDO) {
        BlogArticleContentGetVO articleContentGetVO = BeanMapperComponent
                .map(articleContentDO, BlogArticleContentGetVO.class);
        if (null != articleContentGetVO.getState()) {
            articleContentGetVO.setStatus(StateEnum.getValueByKey(articleContentGetVO.getState()));
        }
        if (null != articleContentGetVO.getGmtCreateIp()) {
            articleContentGetVO.setIp(IpUtils.intToIpv4String(articleContentGetVO.getGmtCreateIp()));
        }
        return articleContentGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogArticleContentServiceImpl class

/* End of file BlogArticleContentServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/cms/impl/BlogArticleContentServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
