/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.event;


import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.admin.common.enums.ScheduleJobOperationTypeEnum;
import wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.service.admin.system.AdminRequestLogService;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;

/**
 * 事件监听器
 *
 * @author ErYang
 */
@Component
public class AdminControllerEventListener {

    private final Scheduler scheduler;

    private final AdminRequestLogService adminRequestLogService;

    @Autowired
    public AdminControllerEventListener(AdminRequestLogService adminRequestLogService, Scheduler scheduler) {
        this.adminRequestLogService = adminRequestLogService;
        this.scheduler = scheduler;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 请求日志事件
     *
     * @param adminRequestLogAopEvent AdminRequestLogAopEvent
     */
    @Async("adminThreadPoolTaskExecutor")
    @EventListener
    public void saveRequestLog(@NotNull final AdminRequestLogAopEvent adminRequestLogAopEvent) {
        if (null != adminRequestLogAopEvent.getAdminRequestLogBO()) {
            adminRequestLogService.save(adminRequestLogAopEvent.getAdminRequestLogBO());
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务 事件
     *
     * @param scheduleJobOperationEvent ScheduleJobOperationEvent
     */
    @Async("adminThreadPoolTaskExecutor")
    @EventListener
    public void scheduleJobOperationEvent(@NotNull final ScheduleJobOperationEvent scheduleJobOperationEvent) {
        // 类型
        ScheduleJobOperationTypeEnum type = scheduleJobOperationEvent.getScheduleJobOperationTypeEnum();
        if (null != type) {
            // 定时任务
            AdminScheduleJobGetVO scheduleJobVO = null;

            Object object = scheduleJobOperationEvent.getSource();
            if (object instanceof AdminScheduleJobGetVO) {
                scheduleJobVO = (AdminScheduleJobGetVO) object;
            }
            if (null != scheduleJobVO) {
                if (type == ScheduleJobOperationTypeEnum.ADD) {
                    // 新增
                    QuartzScheduleUtils.addScheduleJob(scheduler, scheduleJobVO);
                } else if (type == ScheduleJobOperationTypeEnum.UPDATE) {
                    // 编辑
                    QuartzScheduleUtils.editScheduleJob(scheduler, scheduleJobVO);
                } else if (type == ScheduleJobOperationTypeEnum.RUN) {
                    // 运行
                    QuartzScheduleUtils.runScheduleJob(scheduler, scheduleJobVO);
                } else if (type == ScheduleJobOperationTypeEnum.PAUSE) {
                    // 暂停
                    QuartzScheduleUtils.pauseScheduleJob(scheduler, scheduleJobVO);
                } else if (type == ScheduleJobOperationTypeEnum.RESUME) {
                    // 恢复
                    QuartzScheduleUtils.resumeScheduleJob(scheduler, scheduleJobVO);
                } else if (type == ScheduleJobOperationTypeEnum.DELETE) {
                    // 删除
                    QuartzScheduleUtils.deleteScheduleJob(scheduler, scheduleJobVO);
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminControllerEventListener class

/* End of file AdminControllerEventListener.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/event/AdminControllerEventListener.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
