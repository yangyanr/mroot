<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * Mail 验证信息
     */
    var MailValidation = function () {


        var email_pattern = '${I18N("jquery.validation.system.mail.email.pattern")}';

        return {


            getEmail: function () {
                return email_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }

    }();

    // ---------------------------------------------------------------------------------------------------------

</script>
