/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.util;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import java.io.Serializable;

/**
 * shiro session 工具类
 *
 * @author ErYang
 */
public class ShiroSessionUtils {

    /**
     * 移除 key-value
     *
     * @param key String
     */
    public static void removeAttribute(final String key) {
        ShiroSessionUtils.getSession().removeAttribute(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 key-value
     *
     * @param key String
     * @param value Object
     */
    public static void setAttribute(final String key, final Object value) {
        ShiroSessionUtils.getSession().setAttribute(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 key 得到 value
     *
     * @param key String
     *
     * @return Object
     */
    public static Object getAttribute(final String key) {
        return ShiroSessionUtils.getSession().getAttribute(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 shiro sessionId
     *
     * @return sessionId String
     */
    public static Serializable getSessionId() {
        return ShiroSessionUtils.getSession().getId();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 shiro session
     *
     * @return Session
     */
    public static Session getSession() {
        Subject currentSubject = SecurityUtils.getSubject();
        return currentSubject.getSession(true);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ShiroSessionUtils class

/* End of file ShiroSessionUtils.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/util/ShiroSessionUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
