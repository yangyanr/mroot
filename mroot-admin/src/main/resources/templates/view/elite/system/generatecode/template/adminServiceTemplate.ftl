/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${adminServicePackageName}.${classPrefix};


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import ${voPackageName}.${classPrefix}.${classNameLowerCase}.Admin${className}GetVO;

import java.math.BigInteger;
    import java.util.List;

/**
 * 后台 ${classComment} Service 接口
 *
 * @author ErYang
 */
public interface Admin${className}Service {


  /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return Admin${className}GetVO
     */
    Admin${className}GetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 Admin${className}GetVO
     *
     * @param title String 名称
     * @return Admin${className}GetVO
     */
    Admin${className}GetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 Admin${className}GetVO
     *
     * @param sole String 标识
     * @return Admin${className}GetVO
     */
    Admin${className}GetVO getBySole(@NotNull final String sole);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 ${classComment}
     *
     * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO
     * @return boolean
     */
    boolean save(@NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 ${classComment}
     *
     * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO
     * @return boolean
     */
    boolean update(@NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean recoverById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<Admin${className}GetVO>
    * @param admin${className}GetVO Admin${className}GetVO   实体类 查询条件
    * @param orderByField   String 排序字段
    * @param isAsc  boolean 是否正序 默认正序
    *
    * @return IPage<Admin${className}GetVO>
    */
    IPage<Admin${className}GetVO> list2page(@NotNull final Page<Admin${className}GetVO> pageAdmin,
    @NotNull final Admin${className}GetVO admin${className}GetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
    * Hibernate Validation 验证
    * @param admin${className}GetVO Admin${className}GetVO
    *
    * @return String
    */
    String validation${className}(@NotNull final Admin${className}GetVO admin${className}GetVO);

    // -------------------------------------------------------------------------------------------------

    /**
    * 判断对象的属性值是否唯一
    *
    * 在修改对象的情景下
    * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
    *
    * @param property String 字段
    * @param newValue Object 新值
    * @param oldValue Object 旧值
    * @return boolean true (不存在)/false(存在)
    */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
    @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

                /**
                * 得到  Admin${className}GetVO 列表
                * @param admin${className}GetVO Admin${className}GetVO
                * @return List
                */
                List<Admin${className}GetVO> list(@NotNull final Admin${className}GetVO admin${className}GetVO);

                    // -------------------------------------------------------------------------------------------------

                    /**
                    * 得到所有  Admin${className}GetVO 集合
                    * @param admin${className}GetVO  Admin${className}GetVO 查询条件
                    * @param count int 数量
                    * @param column String 排序字段
                    * @param isAsc boolean 是否正序
                    * @return List 集合
                    */
                    List<Admin${className}GetVO> list(final Admin${className}GetVO admin${className}GetVO, final int count,
                        @NotNull final String column, final boolean isAsc);

                        // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Admin${className}Service interface

/* End of file Admin${className}Service.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/${classPrefix}/Admin${className}.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
